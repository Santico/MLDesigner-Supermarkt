defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { evaluation }
  domain      { SDF }
  author      { Mission Level Design GmbH }
  version     { 0.0 09/06/2012 }
  copyright   { Mission Level Design GmbH }

  desc
  {
    checks the input for being lower than 1. Determines the ratio between lower and higher than 1 and calculates PI with the ratio.
It uses the knowledge, that a given point in a field from [0,0] to [1,1] is in the unit circle, if the derivate of the pythagorean theorem sqrt (x^2+y^2) <1  is fulfilled.
Therefore the ratio of points from the unit circle will be approximating to PI/4. Or the approximation of Pi will be the ratio multiplied with 4.

  }


  input
  {
    name { r }
    type { float }
    desc { "distance to the point of origin" }
  }

  output
  {
    name { PI_approx }
    type { float }
    desc { "An approximation of Pi" }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  acknowledge { }

  location { }

  hinclude { }

  ccinclude { }

  constructor
  {
  }

  protected
  {
    int innerPoint;
    int iteration;
    double ratio;
  }

  setup
  {
    innerPoint = 0;
    iteration = 0;
    ratio = 0;
  }

  begin
  {
  }

  go
  {
    iteration++;
    double distance = r%0;
    if (distance <= 1)
    {
      innerPoint++;
    }
   ratio = (double) innerPoint / (double) iteration;
   PI_approx%0 << ratio*4;
  }

  wrapup
  {
  }

  cleanup
  {
  }

  destructor
  {
  }

}
