#ifndef _FSMOnOff_FSMStar_h
#define _FSMOnOff_FSMStar_h 1
// header file generated from FSMOnOff_FSMStar.fsm by ptlang

#ifndef CYGWIN
#ifdef __GNUC__
#pragma interface
#endif
#endif

#include "FSMStar.h"
#include "kernel/Memory.h"
#include "FSMState.h"
#include "FSMTransition.h"

class FSMOnOff_FSMStar : public FSMStar
{
public:
	FSMOnOff_FSMStar();
	/* virtual */ Block* makeNew() const;
	/* virtual */ int isA(const char*) const;
	/* virtual */ const char* className() const;
	/* virtual */ Block& setBlock(const char* s, Block* parent = NULL);
	OutFSMPort Output;
	InFSMPort Button;
	FSMState TopLevel;
	FSMState State0;
	FSMState State1;
	FSMTransition Transition0;
	FSMTransition Transition1;
	void Transition0_Action ();
	void Transition1_Action ();

protected:
	Memory CurrentState;

};
#endif
