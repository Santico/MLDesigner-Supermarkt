static const char file_id[] = "FSMOnOff_FSMStar.fsm";
// .cc file generated from FSMOnOff_FSMStar.fsm by ptlang

#ifndef CYGWIN
#ifdef __GNUC__
#pragma implementation
#endif
#endif

// needed for object file version handling
static char __version__[] = "__MLDesignerVersion__3.1.0";

#include "FSMOnOff_FSMStar.h"
#include "kernel/DsHandler.h"
#include "kernel/SimControl.h"

const char *star_nm_FSMOnOff_FSMStar = "FSMOnOff_FSMStar";

ISA_FUNC(FSMOnOff_FSMStar,FSMStar);

Block* FSMOnOff_FSMStar :: makeNew() const { LOG_NEW; return new FSMOnOff_FSMStar;}

Block& FSMOnOff_FSMStar::setBlock(const char* s, Block* parent)
{
  Block& tBlock = FSMStar::setBlock(s,parent);
# ifdef COMPILE_WITH_DEBUG
    Error::warn(*this,"Primitive was compiled with debug information. This may lead to performance problems.");
# endif
  return tBlock;
}


FSMOnOff_FSMStar::FSMOnOff_FSMStar ()
{
	// needed for object file version handling
	char* tDummy = __version__; tDummy++;
	addPort(Output.setPort("Output",this,STRINGTYPE));
	addPort(Button.setPort("Button",this,FLOATTYPE));
	addMemory(CurrentState.setMemory("CurrentState",this,ArgScope::External, "_2_3_FSM:Root.ENUM.OnOff_FSMCS"));
	addFSMObject(TopLevel.setState("TopLevel","","",0,&State0));
	addFSMObject(State0.setState("State0","off","",&TopLevel,0));
	addFSMObject(State1.setState("State1","on","",&TopLevel,0));
	addFSMObject(Transition0.setTransition("Transition0","$Button",&State0,&State1,0,0));
	addFSMObject(Transition1.setTransition("Transition1","$Button",&State1,&State0,0,0));

# line 84 "FSMOnOff_FSMStar.fsm"
DECLARE_ACTION(FSMOnOff_FSMStar,Transition0_Action);
DECLARE_ACTION(FSMOnOff_FSMStar,Transition1_Action);
setTopLevelState(&TopLevel);
setCurrentStateMemory(&CurrentState);
}


void FSMOnOff_FSMStar::Transition0_Action ()
{
# line 97 "FSMOnOff_FSMStar.fsm"
# line 1 "Transition from State 'off' to State 'on': Action"
WriteOutput(Output, "On");
}


void FSMOnOff_FSMStar::Transition1_Action ()
{
# line 109 "FSMOnOff_FSMStar.fsm"
# line 1 "Transition from State 'on' to State 'off': Action"
WriteOutput(Output, "Off");
}


// prototype instance for known block list
static FSMOnOff_FSMStar proto;
static RegisterBlock registerBlock(proto,"OnOff_FSMStar");
