/* This file was automaticaly generated. */
#ifndef _2_3_FSM_OnOff_FSMCS_h
#define _2_3_FSM_OnOff_FSMCS_h

#include "kernel/DsHandler.h"
#include "kernel/EnumType.h"

/* Data structure class definition. */
/* The namespace is generated from the library name. */
namespace _2_3_FSM
{
class OnOff_FSMCSR : public ENUMR
{
  public:
    //constructors
    DEFINEENUMREFCONSTRUCTORS(OnOff_FSMCS,ENUM)

    /*  Enumeration elements. */
    enum
    {
      off = 0,
      on = 1
    };

  /* Protected section */
  private:
    DEFINENEWVALUE("_2_3_FSM:Root.ENUM.OnOff_FSMCS");

};// class definition
} // namespace

#endif
