/* This file was automaticaly generated. */
#ifndef _2_3_FSM_TestCS_h
#define _2_3_FSM_TestCS_h

#include "kernel/DsHandler.h"
#include "kernel/EnumType.h"

/* Data structure class definition. */
/* The namespace is generated from the library name. */
namespace _2_3_FSM
{
class TestCSR : public ENUMR
{
  public:
    //constructors
    DEFINEENUMREFCONSTRUCTORS(TestCS,ENUM)

    /*  Enumeration elements. */
    enum
    {
      State0 = 0
    };

  /* Protected section */
  private:
    DEFINENEWVALUE("_2_3_FSM:Root.ENUM.TestCS");

};// class definition
} // namespace

#endif
