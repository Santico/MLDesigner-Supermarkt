<?xml version='1.0' encoding='ISO-8859-1' ?>
<model gid="0x51629f3d6657f954000002a400000c14" name="system" version="3.1.0" xmlns="http://www.mldesigner.com/mld" xmlns:svg="http://www.w3.org/2000/svg">
  <svg:svg height="100%" width="100%">
    <property class="String" name="Bounding visible" value="false"/>
  </svg:svg>
  <parameter attributes="A_CONSTANT" gid="0x4ffffcbbaf68b1c40000004200000e24" name="GlobalSeed" scope="External" type="int" value="1234567890"/>
  <parameter attributes="A_CONSTANT" gid="0x4ffffcbbaf68b1c40000004300000e24" name="RunLength" scope="External" type="float" value="50">
    <property class="String" name="Description" value="Determines the number of cycles for non-timed domains or the time to run in seconds for timed domains. The maximum value is MAX_INT, i.e., 2^15-1. Value -1 can be used to define a pseudo-infinite runlength for domains that support the EndCondition feature by primitives."/>
  </parameter>
  <property class="String" name="Logical Name" value="system"/>
  <property class="String" name="Version" value="0.0 07/13/2012"/>
  <property class="String" name="Copyright" value="Mission Level Design GmbH"/>
  <property class="String" name="Author" value="Mission Level Design GmbH"/>
  <property class="String" name="hidden" value="no"/>
  <director class="DE" name="DE">
    <target class="default-DE">
      <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x4ffffcbbaf68b1c40000004400000e24" name="timeScale" scope="External" type="float" value="1.0">
        <property class="String" name="Description" value="Relative time scale for interface with another timed domain"/>
      </parameter>
      <parameter attributes="A_CONSTANT|A_SETTABLE" enumindices="" enumlabels="Calendar Queue Scheduler,Mutable Calendar Queue Scheduler,Priority Free Scheduler,Priority scheduler,Resource Contention scheduler,Simple DE scheduler" enumvalues="0,1,3,4,2,5" gid="0x4ffffcbbaf68b1c40000004500000e24" name="usedScheduler" scope="External" type="enum" value="3">
        <property class="String" name="Description" value="Specifies used DE scheduler, default is Priority Free Scheduler.&#10;"/>
      </parameter>
    </target>
  </director>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-251,180"/>
      <property class="String" name="Text" value="The DE domain is a mature domain using an event-driven model of computation. In this domain, particles carry time stamps,&#10;and represent events that occur at arbitrary points in simulated time. Events are processed in chronological order. MLDesigner&#10;provides a variety of simulation schedulers (Calendar Queue, Mutable Calendar Queue, Priority Free, Resource Contention,&#10;Simple Discrete Event)&#10;&#10;DE schedulers maintain an event queue, which is a list of events sorted chronologically by time stamp. The scheduler selects&#10;the next event on the list, and determines which primitive should be fired to process the event. The difference between the&#10;efficient calendar queue scheduler and the naive simple scheduler is in the efficiency with which this queue is updated and&#10;accessed. Considerable effort was put into consistent and predictable handling of simultaneous events.&#10;"/>
      <property class="String" name="Alignment" value="centerleft"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-235,94"/>
      <property class="String" name="Font" value="Helvetica:12::::"/>
      <property class="String" name="Text" value="Notes"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-215,254"/>
      <property class="String" name="Font" value="Helvetica:12::::"/>
      <property class="String" name="Text" value="Description"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-248,281"/>
      <property class="String" name="Text" value="This system adds only two float values. The 'Clock' is needed to produce events which trigger the 'Ramp' and the 'GenFloatConst'.&#10;Every produced event goes on till it reaches a Sink or it have to wait for another event."/>
      <property class="String" name="Alignment" value="centerleft"/>
    </svg:svg>
  </label>
  <import name="_2_1_DE" url="../_2_1_DE.mml" urlgid="0x5163e32f6657f9540000048c00000704"/>
  <entity class="$MLD/MLD_Libraries/DE/Sinks/Xgraph/Xgraph.mml" classgid="0x3dac0a218b38f651000000f700003aad" gid="0x500000eeaf68b1c40000006700000b28" name="Xgraph#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="236,-16"/>
      <property class="String" name="Label" value="Xgraph"/>
    </svg:svg>
    <property class="String" name="Description" value="Generate a plot with the xgraph program."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68518b38f65100007c1b00007914" gid="0x500000eeaf68b1c40000006f00000b28" name="title" scope="External" type="string" value="">
      <property class="String" name="Description" value="Graph title."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68518b38f65100007c1c00007914" gid="0x500000eeaf68b1c40000007000000b28" name="saveFile" scope="External" type="filename" value="">
      <property class="String" name="Description" value="File to save xgraph input."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68518b38f65100007c1d00007914" gid="0x500000eeaf68b1c40000007100000b28" name="options" scope="External" type="string" value="-bb -tk -P =800x400">
      <property class="String" name="Description" value="Command line options for xgraph."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" enumindices=",," enumlabels="None,Iterations,Paramsets" enumvalues="XG_CUM_NONE,XG_CUM_ITERS,XG_CUM_PMSET" formalgid="0x3dcf68518b38f65100007c1e00007914" gid="0x500000eeaf68b1c40000007200000b28" name="Cumulation" scope="External" type="enum" value="XG_CUM_NONE">
      <property class="String" name="Description" value="Specifies the cumulation level for the graph display:&#10;None - a graph display for each iteration;&#10;Iterations - a graph display for all iterations of a parameter set;&#10;Paramsets - a single graph display for the whole simulation."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x4a79702242787b4f0000005f00005fa1" gid="0x500000eeaf68b1c40000007300000b28" name="Enabled" scope="External" type="int" value="YES">
      <property class="String" name="Description" value="Enable or disable collecting/displaying data and writing files."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf68518b38f65100007c1f00007914" gid="0x500000eeaf68b1c40000006e00000b28" name="input" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-56,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/Sources/Clock/Clock.mml" classgid="0x3dac05cc8b38f651000000a600003aad" gid="0x50000388af68b1c40000015c00000b28" name="Clock#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-148,-16"/>
      <property class="String" name="Label" value="Clock"/>
    </svg:svg>
    <property class="String" name="Description" value="&#10;Generate events at regular intervals, starting at time zero.&#10; "/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68858b38f651000083a100007914" gid="0x50000388af68b1c40000015e00000b28" name="interval" scope="External" type="float" value="1.0">
      <property class="String" name="Description" value="The interval of events."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68858b38f651000083a200007914" gid="0x50000388af68b1c40000015f00000b28" name="magnitude" scope="External" type="float" value="1.0">
      <property class="String" name="Description" value="The value of the output particles generated."/>
    </parameter>
    <port class="float" formalgid="0x3dcf68858b38f651000083a300007914" gid="0x50000388af68b1c40000015d00000b28" name="output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="24,-36"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-10,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/Sources/Ramp/Ramp.mml" classgid="0x3dac05cd8b38f651000000ae00003aad" gid="0x50000391af68b1c40000016100000b28" name="Ramp#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Label" value="Ramp&#10;Step: 1"/>
      <property class="String" name="Position" value="-4,-64"/>
    </svg:svg>
    <property class="String" name="Description" value="Produce an output event with a monotic value when stimulated by an input event. The value of the output event starts at &quot;value&quot; and increases by &quot;step&quot; each time the star fires. The value of the input is ignored."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68878b38f651000083c200007914" gid="0x50000391af68b1c40000016700000b28" name="value" scope="External" type="float" value="0.0">
      <property class="String" name="Description" value="Starting and current state of the ramp."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68878b38f651000083c300007914" gid="0x50000391af68b1c40000016800000b28" name="step" scope="External" type="float" value="1.0">
      <property class="String" name="Description" value="Size of the ramp increments."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf68878b38f651000083c400007914" gid="0x50000391af68b1c40000016500000b28" name="input" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-56,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
    </port>
    <port class="float" formalgid="0x3dcf68878b38f651000083c500007914" gid="0x50000391af68b1c40000016600000b28" name="output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="56,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/NumberGenerators/GenFloatConst/GenFloatConst.mml" classgid="0x3daa6f7f8b38f6510000012b00004dd6" gid="0x51653fe16657f9540000016300000ab4" name="GenFloatConst#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-4,36"/>
      <property class="String" name="Label" value="GenFloatConst&#10;Value: 1"/>
    </svg:svg>
    <property class="String" name="Description" value="Outputs a constant float value"/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf681e8b38f651000070a600007914" gid="0x51653fe16657f9540000016800000ab4" name="Value" scope="External" type="float" value="1.0">
      <property class="String" name="Description" value="The constant value."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf681e8b38f651000070a700007914" gid="0x51653fe16657f9540000016600000ab4" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
    </port>
    <port class="float" formalgid="0x3dcf681e8b38f651000070a800007914" gid="0x51653fe16657f9540000016700000ab4" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="44,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/Arithmetic/AddFloat/AddFloat.mml" classgid="0x3dac0a1c8b38f651000000de00003aad" gid="0x516540046657f9540000017d00000ab4" name="AddFloat#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="136,-16"/>
      <property class="String" name="Label" value="AddFloat"/>
    </svg:svg>
    <property class="String" name="Description" value="Computes the sum of the two input values."/>
    <port class="float" formalgid="0x3dcf68138b38f65100006f9d00007914" gid="0x516540046657f9540000017f00000ab4" name="Input1" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,-124"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
    </port>
    <port class="float" formalgid="0x3dcf68138b38f65100006f9e00007914" gid="0x516540046657f9540000018000000ab4" name="Input2" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,-84"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,40"/>
      </svg:svg>
    </port>
    <port class="float" formalgid="0x3dcf68138b38f65100006f9f00007914" gid="0x516540046657f9540000018100000ab4" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="44,-100"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,20"/>
      </svg:svg>
    </port>
  </entity>
  <relation name="Relation2">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-124,-16 -52,36 -32,-64 -72,-16 -72,36 -72,-64"/>
      <property class="String" name="Edges" value="0,3 1,4 2,5 3,5 3,4"/>
    </svg:svg>
  </relation>
  <relation name="Relation1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="176,-16 200,-16"/>
      <property class="String" name="Edges" value="0,1"/>
    </svg:svg>
  </relation>
  <relation name="Relation3">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="24,-64 96,-20 60,-64 60,-20"/>
      <property class="String" name="Edges" value="0,2 1,3 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation4">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="44,36 96,-8 60,-8 60,36"/>
      <property class="String" name="Edges" value="0,3 1,2 2,3"/>
    </svg:svg>
  </relation>
  <link port="GenFloatConst#1.Trigger" portgid="0x51653fe16657f9540000016600000ab4" relation="Relation2"/>
  <link port="Clock#1.output" portgid="0x50000388af68b1c40000015d00000b28" relation="Relation2"/>
  <link port="Ramp#1.input" portgid="0x50000391af68b1c40000016500000b28" relation="Relation2"/>
  <link port="AddFloat#1.Output" portgid="0x516540046657f9540000018100000ab4" relation="Relation1"/>
  <link port="Xgraph#1.input" portgid="0x500000eeaf68b1c40000006e00000b28" relation="Relation1"/>
  <link port="Ramp#1.output" portgid="0x50000391af68b1c40000016600000b28" relation="Relation3"/>
  <link port="AddFloat#1.Input1" portgid="0x516540046657f9540000017f00000ab4" relation="Relation3"/>
  <link port="GenFloatConst#1.Output" portgid="0x51653fe16657f9540000016700000ab4" relation="Relation4"/>
  <link port="AddFloat#1.Input2" portgid="0x516540046657f9540000018000000ab4" relation="Relation4"/>
</model>

