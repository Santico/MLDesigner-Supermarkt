<?xml version='1.0' encoding='ISO-8859-1' ?>
<model gid="0x517a31fa6657f954000005160000066c" name="system" version="3.1.0" xmlns="http://www.mldesigner.com/mld" xmlns:svg="http://www.w3.org/2000/svg">
  <svg:svg height="100%" width="100%">
    <property class="String" name="Bounding visible" value="false"/>
  </svg:svg>
  <parameter attributes="A_CONSTANT" gid="0x50042501af68b1c4000008d000000ff0" name="GlobalSeed" scope="External" type="int" value="1234567890"/>
  <parameter attributes="A_CONSTANT" gid="0x50042501af68b1c4000008d100000ff0" name="RunLength" scope="External" type="float" value="-1">
    <property class="String" name="Description" value="Determines the number of cycles for non-timed domains or the time to run in seconds for timed domains. The maximum value is MAX_INT, i.e., 2^15-1. Value -1 can be used to define a pseudo-infinite runlength for domains that support the EndCondition feature by primitives."/>
  </parameter>
  <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x50044575af68b1c40000133c00000fbc" name="speed_modificator" scope="Internal" type="int" value="-18">
    <property class="String" name="Description" value="defines the power of 2 to which the simulation is boosted"/>
  </parameter>
  <property class="String" name="Logical Name" value="system"/>
  <property class="String" name="Version" value="0.0 07/16/2012"/>
  <property class="String" name="Copyright" value="Mission Level Design GmbH"/>
  <property class="String" name="Author" value="Mission Level Design GmbH"/>
  <property class="String" name="hidden" value="no"/>
  <director class="DE" name="DE">
    <target class="default-DE">
      <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x50042501af68b1c4000008d500000ff0" name="timeScale" scope="External" type="float" value="1">
        <property class="String" name="Description" value="Relative time scale for interface with another timed domain"/>
      </parameter>
      <parameter attributes="A_CONSTANT|A_SETTABLE" enumindices="" enumlabels="Calendar Queue Scheduler,Mutable Calendar Queue Scheduler,Priority Free Scheduler,Priority scheduler,Resource Contention scheduler,Simple DE scheduler" enumvalues="0,1,3,4,2,5" gid="0x50042501af68b1c4000008d600000ff0" name="usedScheduler" scope="External" type="enum" value="5">
        <property class="String" name="Description" value="Specifies used DE scheduler, default is Priority Free Scheduler.&#10;"/>
      </parameter>
    </target>
  </director>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="57,350"/>
      <property class="String" name="Font" value="Helvetica:12::::"/>
      <property class="String" name="Text" value="Notes"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="77,470"/>
      <property class="String" name="Font" value="Helvetica:12::::"/>
      <property class="String" name="Text" value="Description"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="42,547"/>
      <property class="String" name="Alignment" value="centerleft"/>
      <property class="String" name="Text" value="This system is a simple interactive example. There are three slider whose values are outputted by&#10;'TKText' primitive. &#10;&#10;Without the clock, no events will be generated and the simulation will be finished before the user can&#10;produce an event. The low interval for the clock slows the simulation to a level, that provides&#10;controllability to the user.&#10;&#10;In the system properties tab you can see, that the 'runlength' is set to -1 factual an infinite runtime."/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="48,368"/>
      <property class="String" name="Alignment" value="topleft"/>
      <property class="String" name="Text" value="'TKSlider' is an interactive on-screen scale slider. For each change an output is generated which is&#10;determined by the position of the slider. Several &quot;slider&quot; can be summarized in a control panel.&#10;You can use more than one control panel in a system by using the 'put_in_control_panel'&#10;parameter.."/>
    </svg:svg>
  </label>
  <import name="_4_1_User_activity" url="../_4_1_User_activity.mml" urlgid="0x51629f446657f954000002b400000c14"/>
  <entity class="$MLD/MLD_Libraries/DE/Sources/Clock/Clock.mml" classgid="0x3dac05cc8b38f651000000a600003aad" gid="0x50042504af68b1c40000090c00000ff0" name="Clock#1">
    <svg:svg height="0" width="0">
      <property class="String" name="Position" value="452,272"/>
      <property class="String" name="ZOrder" value="0"/>
      <property class="String" name="BackgroundColor" value="#ffffff"/>
      <property class="String" name="Label" value="Clock&#10;(KeepAlive)"/>
    </svg:svg>
    <property class="String" name="Description" value="&#10;Generate events at regular intervals, starting at time zero.&#10; "/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68858b38f651000083a100007914" gid="0x50042504af68b1c40000090a00000ff0" name="interval" scope="External" type="float" value="pow(2,$speed_modificator)">
      <property class="String" name="Description" value="The interval of events."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68858b38f651000083a200007914" gid="0x50042504af68b1c40000090b00000ff0" name="magnitude" scope="External" type="float" value="1.0">
      <property class="String" name="Description" value="The value of the output particles generated."/>
    </parameter>
    <port class="float" formalgid="0x3dcf68858b38f651000083a300007914" gid="0x50042504af68b1c40000090d00000ff0" name="output" terminated="true" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Position" value="28,0"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="32,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/TclTk/TkSlider/TkSlider.output=1.mml" classgid="0x3dac15808b38f651000007c900003aad" gid="0x517a2c356657f954000004220000066c" name="TkSlider.output=1#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="132,172"/>
      <property class="String" name="Label" value="slider 1 in control panel"/>
    </svg:svg>
    <property class="String" name="Description" value="Output a value determined by an interactive on-screen scale slider."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68988b38f6510000858400007914" gid="0x517a2c356657f9540000041d0000066c" name="low" scope="External" type="float" value="0.0">
      <property class="String" name="Description" value="The low end of the scale."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68998b38f6510000858500007914" gid="0x517a2c356657f9540000041e0000066c" name="high" scope="External" type="float" value="5">
      <property class="String" name="Description" value="The high end of the scale."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68998b38f6510000858600007914" gid="0x517a2c356657f9540000041f0000066c" name="value" scope="External" type="float" value="0.0">
      <property class="String" name="Description" value="The starting value to send to the output."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68998b38f6510000858700007914" gid="0x517a2c356657f954000004200000066c" name="identifier" scope="External" type="string" value="Slider 1">
      <property class="String" name="Description" value="The string to identify the slider in the control panel."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68998b38f6510000858800007914" gid="0x517a2c356657f954000004210000066c" name="put_in_control_panel" scope="External" type="int" value="YES">
      <property class="String" name="Description" value="Specifies whether to put the Tk widgets in the control panel (vs. their own window)."/>
    </parameter>
    <port derivegid="0x3dcf688d8b38f6510000847100007914" formalgid="0x3dcf68988b38f6510000858300007914" gid="0x517a2c356657f954000004230000066c" name="output#1">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="24,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-10,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Any outputs obtained from Tcl"/>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/TclTk/TkSlider/TkSlider.output=1.mml" classgid="0x3dac15808b38f651000007c900003aad" gid="0x517a2c356657f954000004290000066c" name="TkSlider.output=1#2">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="132,208"/>
      <property class="String" name="Label" value="slider 2 in control panel"/>
    </svg:svg>
    <property class="String" name="Description" value="Output a value determined by an interactive on-screen scale slider."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68988b38f6510000858400007914" gid="0x517a2c356657f954000004240000066c" name="low" scope="External" type="float" value="0.0">
      <property class="String" name="Description" value="The low end of the scale."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68998b38f6510000858500007914" gid="0x517a2c356657f954000004250000066c" name="high" scope="External" type="float" value="5">
      <property class="String" name="Description" value="The high end of the scale."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68998b38f6510000858600007914" gid="0x517a2c356657f954000004260000066c" name="value" scope="External" type="float" value="0.0">
      <property class="String" name="Description" value="The starting value to send to the output."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68998b38f6510000858700007914" gid="0x517a2c356657f954000004270000066c" name="identifier" scope="External" type="string" value="Slider 2">
      <property class="String" name="Description" value="The string to identify the slider in the control panel."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68998b38f6510000858800007914" gid="0x517a2c356657f954000004280000066c" name="put_in_control_panel" scope="External" type="int" value="YES">
      <property class="String" name="Description" value="Specifies whether to put the Tk widgets in the control panel (vs. their own window)."/>
    </parameter>
    <port derivegid="0x3dcf688d8b38f6510000847100007914" formalgid="0x3dcf68988b38f6510000858300007914" gid="0x517a2c356657f9540000042a0000066c" name="output#1">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="24,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-10,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Any outputs obtained from Tcl"/>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/TclTk/TkSlider/TkSlider.output=1.mml" classgid="0x3dac15808b38f651000007c900003aad" gid="0x517a2c356657f954000004300000066c" name="TkSlider.output=1#3">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="132,244"/>
      <property class="String" name="Label" value="slider 3 extra panel"/>
      <property class="String" name="Size" value="129,33"/>
    </svg:svg>
    <property class="String" name="Description" value="Output a value determined by an interactive on-screen scale slider."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68988b38f6510000858400007914" gid="0x517a2c356657f9540000042b0000066c" name="low" scope="External" type="float" value="0.0">
      <property class="String" name="Description" value="The low end of the scale."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68998b38f6510000858500007914" gid="0x517a2c356657f9540000042c0000066c" name="high" scope="External" type="float" value="5">
      <property class="String" name="Description" value="The high end of the scale."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68998b38f6510000858600007914" gid="0x517a2c356657f9540000042d0000066c" name="value" scope="External" type="float" value="0.0">
      <property class="String" name="Description" value="The starting value to send to the output."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68998b38f6510000858700007914" gid="0x517a2c356657f9540000042e0000066c" name="identifier" scope="External" type="string" value="slider 3">
      <property class="String" name="Description" value="The string to identify the slider in the control panel."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68998b38f6510000858800007914" gid="0x517a2c356657f9540000042f0000066c" name="put_in_control_panel" scope="External" type="int" value="NO">
      <property class="String" name="Description" value="Specifies whether to put the Tk widgets in the control panel (vs. their own window)."/>
    </parameter>
    <port derivegid="0x3dcf688d8b38f6510000847100007914" formalgid="0x3dcf68988b38f6510000858300007914" gid="0x517a2c356657f954000004310000066c" name="output#1">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="24,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-10,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Any outputs obtained from Tcl"/>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/TclTk/TkText/TkText.input=3.mml" classgid="0x3dac10078b38f651000004ac00003aad" gid="0x517a2c416657f9540000044c0000066c" name="TkText.input=3#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="336,208"/>
      <property class="String" name="Label" value="TkText"/>
    </svg:svg>
    <property class="String" name="Description" value="Display the values of the inputs in a separate window,&#10;keeping a specified number of past values in view.&#10;The print method of the input particles is used, so any data&#10;type can be handled."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf689a8b38f651000085c200007914" gid="0x517a2c416657f9540000045b0000066c" name="label" scope="External" type="string" value="">
      <property class="String" name="Description" value="A label to put on the display"/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf689a8b38f651000085c400007914" gid="0x517a2c416657f9540000045c0000066c" name="wait_between_outputs" scope="External" type="int" value="NO">
      <property class="String" name="Description" value="Specify whether to wait for user input between output values"/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf689a8b38f651000085c000007914" gid="0x517a2c416657f9540000045d0000066c" name="number_of_past_values" scope="External" type="string" value="100">
      <property class="String" name="Description" value="Specify how many past values you would like saved"/>
    </parameter>
    <port derivegid="0x3dcf68978b38f6510000855100007914" formalgid="0x3df6136c20169f5b0001c10900001cb5" gid="0x517a2c416657f954000004580000066c" name="input#1">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-24,-12"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="10,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Any number of inputs to feed to Tcl"/>
    </port>
    <port derivegid="0x3dcf68978b38f6510000855100007914" formalgid="0x3df6136c20169f5b0001c12d00001cb5" gid="0x517a2c416657f954000004590000066c" name="input#2">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-24,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="10,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Any number of inputs to feed to Tcl"/>
    </port>
    <port derivegid="0x3dcf68978b38f6510000855100007914" formalgid="0x3df6136c20169f5b0001c12e00001cb5" gid="0x517a2c416657f9540000045a0000066c" name="input#3">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-24,12"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="10,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Any number of inputs to feed to Tcl"/>
    </port>
  </entity>
  <relation name="Relation3">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="200,244 308,220 260,244 260,220"/>
      <property class="String" name="Edges" value="0,2 1,3 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation2">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="200,208 308,208"/>
      <property class="String" name="Edges" value="0,1"/>
    </svg:svg>
  </relation>
  <relation name="Relation1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="200,172 308,196 260,172 260,196"/>
      <property class="String" name="Edges" value="0,2 1,3 2,3"/>
    </svg:svg>
  </relation>
  <link port="TkSlider.output=1#3.output#1" portgid="0x517a2c356657f954000004310000066c" relation="Relation3"/>
  <link port="TkText.input=3#1.input#3" portgid="0x517a2c416657f9540000045a0000066c" relation="Relation3"/>
  <link port="TkSlider.output=1#2.output#1" portgid="0x517a2c356657f9540000042a0000066c" relation="Relation2"/>
  <link port="TkText.input=3#1.input#2" portgid="0x517a2c416657f954000004590000066c" relation="Relation2"/>
  <link port="TkSlider.output=1#1.output#1" portgid="0x517a2c356657f954000004230000066c" relation="Relation1"/>
  <link port="TkText.input=3#1.input#1" portgid="0x517a2c416657f954000004580000066c" relation="Relation1"/>
</model>

