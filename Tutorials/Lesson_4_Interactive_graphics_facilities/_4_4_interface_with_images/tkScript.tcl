### First step is to make sure, that there is actually a window to interact with
set s $ptkControlPanel.gui

if {![winfo exists $s]} {
    toplevel $s
    wm title $s "GUI from scratch"
    wm iconname $s "GUI"
} else {
    wm deiconify $s
    raise $s
}

### global variables come in handy later
set x 24; set y 24; set off 24
# choose image folder path
set dir [file dirname [set ${starID}(tcl_file)]]

########################### PART 1 - Input #####################
### Followed by the invocation of controlable element
## Buttons
# build a part of the window for the buttons
set buttons $s.buttons
catch {destroy $buttons}
frame $buttons -relief groove -bd 3
pack $buttons -padx 3 -pady 3 -expand 1 -fill both

# insert an actual button
button $buttons.button -text "Press me!" -bd 4 -padx 3 -pady 3
# we add a press and release method for this button
bind $buttons.button <ButtonPress-1> "setOutputs_$starID 1"
bind $buttons.button <ButtonRelease-1> "setOutputs_$starID 0"

# insert the button in the button part of the window
pack $buttons.button  -side left\
	-expand 1 -fill both -padx 3 -pady 3 

########################### PART 2: Output ####################

### Now we set up the fixed part of the output

## an additonal part of the window has to be specified
set c $s.canvas
canvas $c -relief groove -bd 4 -height 80 -width 120

# prepare the background
$c create image 0 0 -tag background -anchor nw

## actual text is submitted
$c create text $x $y -anchor sw -text "See the bars" -fill "#BB3333" ; #noticed, that we used the global variables x and y to set a position in this canvas?
# if we want to output an text, we need to define it as tagged structure, where we change the text during runtime
$c create text $x [expr $y+2*$off] -anchor nw -tag output -fill blue ; #colors can be assigned as words or as hex value


## append the canvas to the window we started with
pack append $s $c {top}


### Output changeable parts
## Proceed the input with the special procedure proc goTcl_$starID {starID}
proc goTcl_$starID {starID} {

  ## revoke the output part of the window
  global c

  ##use the global variables we defined in the beginning for this procedure
  global x; global y; global off; global dir
  
 ## read the event on the tcl input port
  set inputVals [grabInputs_$starID]
  set value [lindex $inputVals 0]
 
  #### use it for anything
  $c itemconfigure output -text "Value = $value" 

  ### Here the actual image is defined
  if {$value == 1} {
      set im [image create photo -file $dir/bg1.gif]
    } else {
      set im [image create photo -file $dir/bg2.gif]
    }
  ## use the tag "background" from the begining to change image
  $c itemconfigure background -image $im
}


##### For further understanding in topics of tcl and tk, please read the language specifications. 
##### Which may come in handy when developing own systems
