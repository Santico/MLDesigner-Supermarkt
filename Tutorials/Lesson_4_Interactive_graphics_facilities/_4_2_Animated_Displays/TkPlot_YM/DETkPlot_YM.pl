defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { TkPlot_YM }
  domain      { DE }
  author      { Edward A. Lee }
  version     { $Revision: 1.1 $ $Date: 2013/04/26 09:14:12 $ }
  copyright   { Copyright (c) 1990-1996 The Regents of the University of California.
All rights reserved.
See the file $MLD/copyright for copyright notice,
limitation of liability, and disclaimer of warranty provisions. }

  desc
  {
    Plot Y input(s) vs. time with dynamic updating.
Retracing is done to overlay successive time intervals,
as in an oscilloscope.
Two styles are currently supported: "dot" causes
individual points to be plotted, whereas "connect" causes
connected lines to be plotted. Drawing a box in the plot
will reset the plot area to that outlined by the box.
There are also buttons for zooming in and out, and for
resizing the box to just fit the data in view.
  }

  derived { TkXYPlot }

  defparameter
  {
    name        { repeat_border_points }
    type        { int }
    default     { "YES" }
    desc        { "For better visual continuity, rightmost events are repeated on the left" }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  location { SDF Tcl/Tk library }

  protected
  {
	  double *prevValue;
	  double *prevTime;
	  double *prevXpos;
	  int *prevValueSet;
  }

  constructor
  {
	  // Change default parameter values
	  label.setInitValue("Tk Plot");
	  xTitle.setInitValue("time");
	  xRange.setInitValue("0 100");
	  style.setInitValue("connect");
	  X.setAttributes(P_HIDDEN);
	  prevValue = 0;
	  prevTime = 0;
	  prevXpos = 0;
	  prevValueSet = 0;
  }

  begin
  {
	  delete [] prevValue;
	  delete [] prevTime;
	  delete [] prevXpos;
	  delete [] prevValueSet;
	  prevValue = new double[Y.numberPorts()];
	  prevTime = new double[Y.numberPorts()];
	  prevXpos = new double[Y.numberPorts()];
	  prevValueSet = new int[Y.numberPorts()];
	  for (int i = 0; i < Y.numberPorts(); i++) {
	    prevValueSet[i] = 0;
	    prevTime[i] = 0.0;
	    prevXpos[i] = 0.0;
	  }
          DETkXYPlot::begin();
  }

  destructor
  {
	  delete [] prevValue;
	  delete [] prevTime;
	  delete [] prevXpos;
	  delete [] prevValueSet;
  }

  go
  {
	  InDEMPHIter nexty(Y);
	  InDEPort *py;
	  // The trace number
	  int trace = 0;
	  while ((py = nexty++) != 0) {
	    if (py->dataNew) {
	      double xpos = prevXpos[trace] + arrivalTime - prevTime[trace];
	      if (xpos >= xMax) {
		if ((int)repeat_border_points) {
		  // Plot point off the right edge
		  xyplot.addPoint(xpos,(double)((*py)%0), trace+1);
		  if (prevValueSet[trace]) {
		    // Adjust time for previous point
		    while (prevXpos[trace] > xMin) {
		      prevXpos[trace] -= (xMax - xMin);
		    }
		    // Plot the point off the left edge
		    xyplot.breakPlot(trace+1);
		    xyplot.addPoint(prevXpos[trace],prevValue[trace], trace+1);
                  }
                } else {
		  xyplot.breakPlot(trace+1);
		}
	        while (xpos > xMax) {
		  xpos -= (xMax - xMin);
		}
	      }
	      xyplot.addPoint(xpos,(double)(py->get()), trace+1);
	      prevXpos[trace] = xpos;
	      prevTime[trace] = arrivalTime;
	      prevValue[trace] = (double)((*py)%0);
	      prevValueSet[trace] = 1;
	    }
	    trace++;
	  }
  }

}
