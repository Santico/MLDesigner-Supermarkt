### First step is to make sure, that there is actually a window to interact with
set s $ptkControlPanel.gui

if {![winfo exists $s]} {
    toplevel $s
    wm title $s "GUI from scratch"
    wm iconname $s "GUI"
} else {
    wm deiconify $s
    raise $s
}

### global variables come in handy later
set x 24; set y 24; set w 4; set l 64; set off 24

########################### PART 1 - Input #####################
### Followed by the invocation of controlable element
## Buttons
# build a part of the window for the buttons
set buttons $s.buttons
catch {destroy $buttons}
frame $buttons -relief groove -bd 3
pack $buttons -padx 3 -pady 3 -expand 1 -fill both

# insert an actual button
button $buttons.button -text "Press me!" -bd 4 -padx 3 -pady 3
# we add a press and release method for this button
bind $buttons.button <ButtonPress-1> "setOutputs_$starID 1"
bind $buttons.button <ButtonRelease-1> "setOutputs_$starID 0"

# insert the button in the button part of the window
pack $buttons.button  -side left\
	-expand 1 -fill both -padx 3 -pady 3 

########################### PART 2: Output ####################

### Now we set up the fixed part of the output

## an additonal part of the window has to be specified
set c $s.canvas
canvas $c -relief groove -bd 4 -height 80 -width 120

## actual text is submitted
$c create text $x $y -anchor sw -text "A bar is following" -fill "#BB3333" ; #noticed, that we used the global variables x and y to set a position in this canvas?
# if we want to output an text, we need to define it as tagged structure, where we change the text during runtime
$c create text $x [expr $y+2*$off] -anchor nw -tag output -fill blue ; #colors can be assigned as words or as hex value


## append the canvas to the window we started with
pack append $s $c {top}


### Output changeable parts
## Proceed the input with the special procedure proc goTcl_$starID {starID}
proc goTcl_$starID {starID}  {

  ## revoke the output part of the window
  global c; 

  ##use the global variables we defined in the beginning for this procedure
  global x; global y; global w; global l; global off; 
  
 ## read the event on the tcl input port
  set inputVals [grabInputs_$starID]
  set value [lindex $inputVals 0]

  #### use it for anything
  if {$value == 1} {
  ## creating a hexagon shaped like a bar on a digital watch, we use the offset from the global variable to not overwrite our text
  $c create polygon 	[expr $x] [expr $y+$off] \
		[expr $x+$w] [expr $y-$w+$off]\
		[expr $x+$w+$l] [expr $y-$w+$off]\
		[expr $x+$l+2*$w] [expr $y+$off]\
		[expr $x+$w+$l] [expr $y+$w+$off]\
		[expr $x+$w] [expr $y+$w+$off]\
		-fill green -tag greenbar; #every point in the polygon has be defined by x and y sequence
    } else {
      $c delete greenbar
      # this is used to change between show bar and no bar. The tag which had been definded above is used to determine which object has to be deleted
    }

  ## at last to the text output, we are using the 2nd created text from above via the tag. Creating a new text here would end up in a total mess
  $c itemconfigure output -text "Value = $value" 
}


##### For further understanding in topics of tcl and tk, please read the language specifications. Which may come in handy when developing own systems
