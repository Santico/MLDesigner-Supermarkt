<?xml version='1.0' encoding='ISO-8859-1' ?>
<model gid="0x5178ef5a6657f954000001e400000c58" name="system" version="3.1.0" xmlns="http://www.mldesigner.com/mld" xmlns:svg="http://www.w3.org/2000/svg">
  <svg:svg height="100%" width="100%">
    <property class="String" name="Bounding visible" value="false"/>
  </svg:svg>
  <parameter attributes="A_CONSTANT" gid="0x5178ef5a6657f954000001e600000c58" name="GlobalSeed" scope="External" type="int" value="1234567890"/>
  <parameter attributes="A_CONSTANT" gid="0x5178ef5a6657f954000001e700000c58" name="RunLength" scope="External" type="float" value="100"/>
  <property class="String" name="Logical Name" value="system"/>
  <property class="String" name="Version" value="0.0 04/25/2013"/>
  <property class="String" name="Copyright" value="Mission Level Design GmbH"/>
  <property class="String" name="Author" value="Mission Level Design GmbH"/>
  <property class="String" name="hidden" value="no"/>
  <director class="DE" name="DE">
    <target class="default-DE">
      <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x5178ef5a6657f954000001eb00000c58" name="timeScale" scope="External" type="float" value="1.0">
        <property class="String" name="Description" value="Relative time scale for interface with another timed domain"/>
      </parameter>
      <parameter attributes="A_CONSTANT|A_SETTABLE" enumindices="" enumlabels="Calendar Queue Scheduler,Mutable Calendar Queue Scheduler,Priority Free Scheduler,Priority scheduler,Resource Contention scheduler,Simple DE scheduler" enumvalues="0,1,3,4,2,5" gid="0x5178ef5a6657f954000001ec00000c58" name="usedScheduler" scope="External" type="enum" value="3">
        <property class="String" name="Description" value="Specifies used DE scheduler, default is Priority Free Scheduler.&#10;"/>
      </parameter>
    </target>
  </director>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-290,203"/>
      <property class="String" name="Text" value="A Resource can be used to simulate an item that is shared, such as main memory in a computer or the processing power of a CPU.&#10;In MLDesigner, resources are divided into Quantity Resources and Server Resources.&#10;&#10;A Server Resource is logically the combination of two fundamental components: a queue and a bank of one or more servers. &#10;The data structure and several attributes, such as the requested service time, are together called a transaction. A transaction&#10;immediately moves to a server if one is idle, or may optionally preempt lower priority transactions."/>
      <property class="String" name="Alignment" value="centerleft"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-275,146"/>
      <property class="String" name="Font" value="Helvetica:12::::"/>
      <property class="String" name="Text" value="Notes"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-255,270"/>
      <property class="String" name="Font" value="Helvetica:12::::"/>
      <property class="String" name="Text" value="Description"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-290,310"/>
      <property class="String" name="Text" value="In this system four elements, named 'A', 'B, 'C' and  'D',  with a associated service time are generated. The Input 'TimeDeadline'&#10;is only important if you choose an other 'Server Mechanism', but it needs an input.&#10;The resource is configured to process only one task at once and only another can be in the queue."/>
      <property class="String" name="Alignment" value="centerleft"/>
    </svg:svg>
  </label>
  <import name="_3_4_2_Server_Ressources" url="../_3_4_2_Server_Ressources.mml" urlgid="0x5178e95b6657f9540000018400000c58"/>
  <import name="SystemDS" url="$MLD/lib/system/SystemDS.mml" urlgid="0x3df712670e087361000000130000259f"/>
  <resource gid="0x5178effe6657f9540000022e00000c58" name="Resource" scope="Internal" type="Server">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="28,-196"/>
    </svg:svg>
    <property class="String" name="NumberOfDimensions" value="1"/>
    <property class="String" name="QueueDiscipline" value="First_In_First_Out"/>
    <property class="String" name="QueueRejectMechanism" value="Incoming_DS_Rejected"/>
    <property class="String" name="InitialNumberOfServers" value="1"/>
    <property class="String" name="InitialServiceRateMultiplier" value="1.0"/>
    <property class="String" name="ContextSwitchingOverhead" value="0.0"/>
    <property class="String" name="MaximumOccupancy" value="2"/>
    <property class="String" name="ServerMechanism" value="Dedicated_Server"/>
    <property class="String" name="HardDeadlines" value="no"/>
    <property class="String" name="PreemptDiscipline" value="Dont_Preempt"/>
  </resource>
  <entity class="$MLD/MLD_Libraries/DE/ServerResources/ServiceBasic/ServiceBasic.mml" classgid="0x3dac14a28b38f6510000070900003aad" gid="0x5178ef5d6657f954000001fa00000c58" name="ServiceBasic#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="52,-108"/>
    </svg:svg>
    <property class="String" name="Description" value="This module is used to request service from a Server Resource."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf69a18b38f65100009f1c00007914" gid="0x5178ef5d6657f954000001f500000c58" name="priority" scope="External" type="int" value="0">
      <property class="String" name="Description" value="The priority for each transaction. It must be greater than or equal to zero, zero being the lowest priority."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf69a18b38f65100009f1d00007914" gid="0x5178ef5d6657f954000001f600000c58" name="timeSlice" scope="External" type="float" value="0.0">
      <property class="String" name="Description" value="The Round Robin Time-Slice. This value is only used if the Server Mechanism is Round Robin."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf69a18b38f65100009f1e00007914" gid="0x5178ef5d6657f954000001f700000c58" name="resumeOverhead" scope="External" type="float" value="0.0">
      <property class="String" name="Description" value="The service time wasted when a preempted transaction resumes service."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf69a18b38f65100009f1f00007914" gid="0x5178ef5d6657f954000001f800000c58" name="dimension" scope="External" type="int" value="0">
      <property class="String" name="Description" value="The Dimension of the Resource which each transaction is requesting service from."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" datastruct="SystemDS:Root.ENUM.PreemptResponse" datastructgid="0x3d9b0d861faba0e60000002a00004d2a" formalgid="0x3dcf69a18b38f65100009f2000007914" gid="0x5178ef5d6657f954000001f900000c58" name="preemptResponse" scope="External" type="datastruct">
      <property class="String" name="Description" value="The action taken by a transaction if it is preempted."/>
      <value default="{Resume}" name="SystemDS:Root.ENUM.PreemptResponse" typegid="0x3d9b0d861faba0e60000002a00004d2a"/>
    </parameter>
    <port class="datastruct" datastruct="Root" datastructgid="0xffffffffffffffff0000000100000001" formalgid="0x3dcf69a18b38f65100009f2200007914" gid="0x5178ef5d6657f954000001fc00000c58" name="InArbitraryDS" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-236,-80"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-238,-80"/>
      </svg:svg>
      <property class="String" name="Description" value="The arbitrary data structure associated with the transaction."/>
    </port>
    <port class="float" formalgid="0x3dcf69a18b38f65100009f2300007914" gid="0x5178ef5d6657f954000001fd00000c58" name="ServiceTime" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-236,-56"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-238,-56"/>
      </svg:svg>
      <property class="String" name="Description" value="The amount of service required by the transaction."/>
    </port>
    <port class="datastruct" datastruct="Root" datastructgid="0xffffffffffffffff0000000100000001" formalgid="0x3dcf69a18b38f65100009f2400007914" gid="0x5178ef5d6657f954000001fe00000c58" name="OutArbitraryDS" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="144,4"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="142,0"/>
      </svg:svg>
      <property class="String" name="Description" value="The Arbitrary Data Structure associated with the transaction is placed here when the transaction has received its requested service time."/>
    </port>
    <port class="datastruct" datastruct="Root" datastructgid="0xffffffffffffffff0000000100000001" formalgid="0x3dcf69a18b38f65100009f2500007914" gid="0x5178ef5d6657f954000001ff00000c58" name="RejectDS" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="144,44"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="142,40"/>
      </svg:svg>
      <property class="String" name="Description" value="Data structures associated with rejected transactions are placed here."/>
    </port>
    <port class="float" formalgid="0x49ca5ec4418021900000101400000de6" gid="0x5178ef5d6657f9540000020000000c58" name="TimeDeadline" type="input">
      <svg:svg height="100%" width="100%">
        <property class="String" name="Position" value="-236,156"/>
      </svg:svg>
    </port>
    <resource gid="0x5178ef5d6657f954000001fb00000c58" link="$Resource" name="Resource" scope="External" type="Server">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="32,-52"/>
      </svg:svg>
      <property class="String" name="Description" value="This argument is used to specify which Service modules share the same Server Resource."/>
    </resource>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/Sources/PulseGen/PulseGen.mml" classgid="0x3dac05cd8b38f651000000ad00003aad" gid="0x5178f1096657f9540000023200000c58" name="PulseGen#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-180,-196"/>
      <property class="String" name="Label" value="Event at Time 0"/>
    </svg:svg>
    <property class="String" name="Description" value="&#10;This star generates events with specified values at specified moments.&#10;The events are specified in the &quot;value&quot; array, which consists of&#10;time-value pairs, given in the syntax of complex numbers.  This star&#10;assumes that the time-value pairs appear in chronological order&#10;(ascending with respect to time) and that no simultaneous events&#10;occur (all time values are unique).&#10; "/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68878b38f651000083c000007914" gid="0x5178f1096657f9540000023800000c58" name="value" scope="External" type="complexarray" value="(0,1)"/>
    <port class="float" formalgid="0x3dcf68878b38f651000083c100007914" gid="0x5178f1096657f9540000023700000c58" name="output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="24,-44"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-10,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/StringOperations/ConstStringGen/ConstStringGen.mml" classgid="0x3dac0aaf8b38f651000002d100003aad" gid="0x5179142c6657f95400000aa000000c58" name="ConstStringGen#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-88,-212"/>
      <property class="String" name="Label" value="A"/>
    </svg:svg>
    <property class="String" name="Description" value="Output the given string on each firing."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf688b8b38f6510000845600007914" gid="0x5179142c6657f95400000a9f00000c58" name="Value" scope="External" type="string" value="A">
      <property class="String" name="Description" value="The string to be generated."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf688b8b38f6510000845700007914" gid="0x5179142c6657f95400000aa100000c58" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-25,0"/>
      </svg:svg>
      <property class="String" name="Description" value=" This triggers the primitive."/>
    </port>
    <port class="string" formalgid="0x3dcf688b8b38f6510000845800007914" gid="0x5179142c6657f95400000aa200000c58" name="StringOutput" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="25,0"/>
        <property class="String" name="PortAlign" value="right"/>
      </svg:svg>
      <property class="String" name="Description" value=" The generated string."/>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/NumberGenerators/GenIntConst/GenIntConst.mml" classgid="0x3daa70498b38f6510000017a00004dd6" gid="0x5179142c6657f95400000aa400000c58" name="GenIntConst#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-88,-176"/>
      <property class="String" name="Label" value="3"/>
    </svg:svg>
    <property class="String" name="Description" value="Outputs a constant integer value"/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf681e8b38f6510000709e00007914" gid="0x5179142c6657f95400000aa300000c58" name="Value" scope="External" type="int" value="3">
      <property class="String" name="Description" value="The constant value."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf681e8b38f6510000709f00007914" gid="0x5179142c6657f95400000aa500000c58" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
    </port>
    <port class="int" formalgid="0x3dcf681e8b38f651000070a000007914" gid="0x5179142c6657f95400000aa600000c58" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="44,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/StringOperations/ConstStringGen/ConstStringGen.mml" classgid="0x3dac0aaf8b38f651000002d100003aad" gid="0x5179142c6657f95400000aa800000c58" name="ConstStringGen#2">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-88,-120"/>
      <property class="String" name="Label" value="B"/>
    </svg:svg>
    <property class="String" name="Description" value="Output the given string on each firing."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf688b8b38f6510000845600007914" gid="0x5179142c6657f95400000aa700000c58" name="Value" scope="External" type="string" value="B">
      <property class="String" name="Description" value="The string to be generated."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf688b8b38f6510000845700007914" gid="0x5179142c6657f95400000aa900000c58" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-25,0"/>
      </svg:svg>
      <property class="String" name="Description" value=" This triggers the primitive."/>
    </port>
    <port class="string" formalgid="0x3dcf688b8b38f6510000845800007914" gid="0x5179142c6657f95400000aaa00000c58" name="StringOutput" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="25,0"/>
        <property class="String" name="PortAlign" value="right"/>
      </svg:svg>
      <property class="String" name="Description" value=" The generated string."/>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/NumberGenerators/GenIntConst/GenIntConst.mml" classgid="0x3daa70498b38f6510000017a00004dd6" gid="0x5179142c6657f95400000aac00000c58" name="GenIntConst#2">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-88,-80"/>
      <property class="String" name="Label" value="1"/>
    </svg:svg>
    <property class="String" name="Description" value="Outputs a constant integer value"/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf681e8b38f6510000709e00007914" gid="0x5179142c6657f95400000aab00000c58" name="Value" scope="External" type="int" value="1">
      <property class="String" name="Description" value="The constant value."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf681e8b38f6510000709f00007914" gid="0x5179142c6657f95400000aad00000c58" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
    </port>
    <port class="int" formalgid="0x3dcf681e8b38f651000070a000007914" gid="0x5179142c6657f95400000aae00000c58" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="44,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/StringOperations/ConstStringGen/ConstStringGen.mml" classgid="0x3dac0aaf8b38f651000002d100003aad" gid="0x5179142c6657f95400000ab000000c58" name="ConstStringGen#3">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-88,-36"/>
      <property class="String" name="Label" value="C"/>
    </svg:svg>
    <property class="String" name="Description" value="Output the given string on each firing."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf688b8b38f6510000845600007914" gid="0x5179142c6657f95400000aaf00000c58" name="Value" scope="External" type="string" value="C">
      <property class="String" name="Description" value="The string to be generated."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf688b8b38f6510000845700007914" gid="0x5179142c6657f95400000ab100000c58" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-12,0"/>
      </svg:svg>
      <property class="String" name="Description" value=" This triggers the primitive."/>
    </port>
    <port class="string" formalgid="0x3dcf688b8b38f6510000845800007914" gid="0x5179142c6657f95400000ab200000c58" name="StringOutput" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="12,0"/>
        <property class="String" name="PortAlign" value="right"/>
      </svg:svg>
      <property class="String" name="Description" value=" The generated string."/>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/NumberGenerators/GenIntConst/GenIntConst.mml" classgid="0x3daa70498b38f6510000017a00004dd6" gid="0x5179142c6657f95400000ab400000c58" name="GenIntConst#3">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-88,0"/>
      <property class="String" name="Label" value="3"/>
    </svg:svg>
    <property class="String" name="Description" value="Outputs a constant integer value"/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf681e8b38f6510000709e00007914" gid="0x5179142c6657f95400000ab300000c58" name="Value" scope="External" type="int" value="3">
      <property class="String" name="Description" value="The constant value."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf681e8b38f6510000709f00007914" gid="0x5179142c6657f95400000ab500000c58" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
    </port>
    <port class="int" formalgid="0x3dcf681e8b38f651000070a000007914" gid="0x5179142c6657f95400000ab600000c58" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="44,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/StringOperations/ConstStringGen/ConstStringGen.mml" classgid="0x3dac0aaf8b38f651000002d100003aad" gid="0x5179149a6657f95400000b9400000c58" name="ConstStringGen#4">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-88,52"/>
      <property class="String" name="Label" value="D"/>
    </svg:svg>
    <property class="String" name="Description" value="Output the given string on each firing."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf688b8b38f6510000845600007914" gid="0x5179149a6657f95400000b9300000c58" name="Value" scope="External" type="string" value="D">
      <property class="String" name="Description" value="The string to be generated."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf688b8b38f6510000845700007914" gid="0x5179149a6657f95400000b9500000c58" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-25,0"/>
      </svg:svg>
      <property class="String" name="Description" value=" This triggers the primitive."/>
    </port>
    <port class="string" formalgid="0x3dcf688b8b38f6510000845800007914" gid="0x5179149a6657f95400000b9600000c58" name="StringOutput" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="25,0"/>
        <property class="String" name="PortAlign" value="right"/>
      </svg:svg>
      <property class="String" name="Description" value=" The generated string."/>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/NumberGenerators/GenIntConst/GenIntConst.mml" classgid="0x3daa70498b38f6510000017a00004dd6" gid="0x5179149a6657f95400000b9800000c58" name="GenIntConst#4">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-88,88"/>
      <property class="String" name="Label" value="2"/>
    </svg:svg>
    <property class="String" name="Description" value="Outputs a constant integer value"/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf681e8b38f6510000709e00007914" gid="0x5179149a6657f95400000b9700000c58" name="Value" scope="External" type="int" value="2">
      <property class="String" name="Description" value="The constant value."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf681e8b38f6510000709f00007914" gid="0x5179149a6657f95400000b9900000c58" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
    </port>
    <port class="int" formalgid="0x3dcf681e8b38f651000070a000007914" gid="0x5179149a6657f95400000b9a00000c58" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="44,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/Sources/PulseGen/PulseGen.mml" classgid="0x3dac05cd8b38f651000000ad00003aad" gid="0x517914f36657f95400000bb200000c58" name="PulseGen#3">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-180,-100"/>
      <property class="String" name="Label" value="Event at Time 1"/>
    </svg:svg>
    <property class="String" name="Description" value="&#10;This star generates events with specified values at specified moments.&#10;The events are specified in the &quot;value&quot; array, which consists of&#10;time-value pairs, given in the syntax of complex numbers.  This star&#10;assumes that the time-value pairs appear in chronological order&#10;(ascending with respect to time) and that no simultaneous events&#10;occur (all time values are unique).&#10; "/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68878b38f651000083c000007914" gid="0x517914f36657f95400000bb100000c58" name="value" scope="External" type="complexarray" value="(1,1)"/>
    <port class="float" formalgid="0x3dcf68878b38f651000083c100007914" gid="0x517914f36657f95400000bb300000c58" name="output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="24,-44"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-10,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/Sources/PulseGen/PulseGen.mml" classgid="0x3dac05cd8b38f651000000ad00003aad" gid="0x5179150c6657f95400000bc100000c58" name="PulseGen#4">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-180,-16"/>
      <property class="String" name="Label" value="Event at Time 2"/>
    </svg:svg>
    <property class="String" name="Description" value="&#10;This star generates events with specified values at specified moments.&#10;The events are specified in the &quot;value&quot; array, which consists of&#10;time-value pairs, given in the syntax of complex numbers.  This star&#10;assumes that the time-value pairs appear in chronological order&#10;(ascending with respect to time) and that no simultaneous events&#10;occur (all time values are unique).&#10; "/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68878b38f651000083c000007914" gid="0x5179150c6657f95400000bc000000c58" name="value" scope="External" type="complexarray" value="(2,1)"/>
    <port class="float" formalgid="0x3dcf68878b38f651000083c100007914" gid="0x5179150c6657f95400000bc200000c58" name="output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="24,-44"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-10,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/Sources/PulseGen/PulseGen.mml" classgid="0x3dac05cd8b38f651000000ad00003aad" gid="0x517915256657f95400000bd300000c58" name="PulseGen#5">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-180,68"/>
      <property class="String" name="Label" value="Event at Time 4"/>
    </svg:svg>
    <property class="String" name="Description" value="&#10;This star generates events with specified values at specified moments.&#10;The events are specified in the &quot;value&quot; array, which consists of&#10;time-value pairs, given in the syntax of complex numbers.  This star&#10;assumes that the time-value pairs appear in chronological order&#10;(ascending with respect to time) and that no simultaneous events&#10;occur (all time values are unique).&#10; "/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68878b38f651000083c000007914" gid="0x517915256657f95400000bd200000c58" name="value" scope="External" type="complexarray" value="(4,1)"/>
    <port class="float" formalgid="0x3dcf68878b38f651000083c100007914" gid="0x517915256657f95400000bd400000c58" name="output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="24,-44"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-10,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/TclTk/TkText/TkText.input=1.mml" classgid="0x3dac0ad78b38f6510000030600003aad" gid="0x5179156b6657f95400000c1600000c58" name="TkText.input=1#3">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="176,-128"/>
      <property class="String" name="Label" value="TkText&#10;processed"/>
    </svg:svg>
    <property class="String" name="Description" value="Display the values of the inputs in a separate window,&#10;keeping a specified number of past values in view.&#10;The print method of the input particles is used, so any data&#10;type can be handled."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf689a8b38f651000085af00007914" gid="0x5179156b6657f95400000c1300000c58" name="label" scope="External" type="string" value="processed">
      <property class="String" name="Description" value="A label to put on the display"/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf689a8b38f651000085b100007914" gid="0x5179156b6657f95400000c1400000c58" name="wait_between_outputs" scope="External" type="int" value="NO">
      <property class="String" name="Description" value="Specify whether to wait for user input between output values"/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf689a8b38f651000085ad00007914" gid="0x5179156b6657f95400000c1500000c58" name="number_of_past_values" scope="External" type="string" value="100">
      <property class="String" name="Description" value="Specify how many past values you would like saved"/>
    </parameter>
    <port derivegid="0x3dcf68978b38f6510000855100007914" formalgid="0x3dcf689a8b38f651000085ab00007914" gid="0x5179156b6657f95400000c1700000c58" name="input#1">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-56,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="10,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Any number of inputs to feed to Tcl"/>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/TclTk/TkText/TkText.input=1.mml" classgid="0x3dac0ad78b38f6510000030600003aad" gid="0x5179156b6657f95400000c1b00000c58" name="TkText.input=1#4">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="164,-80"/>
      <property class="String" name="Label" value="TkText&#10;rejected"/>
    </svg:svg>
    <property class="String" name="Description" value="Display the values of the inputs in a separate window,&#10;keeping a specified number of past values in view.&#10;The print method of the input particles is used, so any data&#10;type can be handled."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf689a8b38f651000085af00007914" gid="0x5179156b6657f95400000c1800000c58" name="label" scope="External" type="string" value="rejected">
      <property class="String" name="Description" value="A label to put on the display"/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf689a8b38f651000085b100007914" gid="0x5179156b6657f95400000c1900000c58" name="wait_between_outputs" scope="External" type="int" value="NO">
      <property class="String" name="Description" value="Specify whether to wait for user input between output values"/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf689a8b38f651000085ad00007914" gid="0x5179156b6657f95400000c1a00000c58" name="number_of_past_values" scope="External" type="string" value="100">
      <property class="String" name="Description" value="Specify how many past values you would like saved"/>
    </parameter>
    <port derivegid="0x3dcf68978b38f6510000855100007914" formalgid="0x3dcf689a8b38f651000085ab00007914" gid="0x5179156b6657f95400000c1c00000c58" name="input#1">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-56,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="10,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Any number of inputs to feed to Tcl"/>
    </port>
  </entity>
  <relation name="Relation9">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-132,-196 -104,-212 -104,-176 -108,-196 -108,-212 -108,-176"/>
      <property class="String" name="Edges" value="0,3 1,4 2,5 3,4 3,5"/>
    </svg:svg>
  </relation>
  <relation name="Relation7">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-132,-100 -104,-80 -104,-120 -108,-120 -108,-100 -108,-80"/>
      <property class="String" name="Edges" value="0,4 1,5 2,3 3,4 4,5"/>
    </svg:svg>
  </relation>
  <relation name="Relation6">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-132,-16 -104,0 -104,-36 -108,0 -108,-36 -108,-16"/>
      <property class="String" name="Edges" value="0,5 1,3 2,4 3,5 4,5"/>
    </svg:svg>
  </relation>
  <relation name="Relation5">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-132,68 -104,52 -104,88 -108,68 -108,88 -108,52"/>
      <property class="String" name="Edges" value="0,3 1,5 2,4 3,4 3,5"/>
    </svg:svg>
  </relation>
  <relation name="Relation2">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="104,-100 132,-80 116,-100 116,-80"/>
      <property class="String" name="Edges" value="0,2 1,3 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation3">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="104,-112 136,-128 116,-128 116,-112"/>
      <property class="String" name="Edges" value="0,3 1,2 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation8">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-72,52 -72,-212 -72,-36 -72,-120 0,-120 -52,-212 -52,-36 -52,52 -52,-120"/>
      <property class="String" name="Edges" value="0,7 1,5 2,6 3,8 4,8 5,8 6,7 6,8"/>
    </svg:svg>
  </relation>
  <relation name="Relation1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-72,88 -72,-80 -72,-176 -72,0 0,-108 0,-96 -32,88 -32,0 -32,-96 -32,-80 -32,-176"/>
      <property class="String" name="Edges" value="0,6 1,9 2,10 3,7 4,5 5,8 6,7 7,9 8,10 8,9"/>
    </svg:svg>
  </relation>
  <link port="PulseGen#1.output" portgid="0x5178f1096657f9540000023700000c58" relation="Relation9"/>
  <link port="ConstStringGen#1.Trigger" portgid="0x5179142c6657f95400000aa100000c58" relation="Relation9"/>
  <link port="GenIntConst#1.Trigger" portgid="0x5179142c6657f95400000aa500000c58" relation="Relation9"/>
  <link port="PulseGen#3.output" portgid="0x517914f36657f95400000bb300000c58" relation="Relation7"/>
  <link port="GenIntConst#2.Trigger" portgid="0x5179142c6657f95400000aad00000c58" relation="Relation7"/>
  <link port="ConstStringGen#2.Trigger" portgid="0x5179142c6657f95400000aa900000c58" relation="Relation7"/>
  <link port="PulseGen#4.output" portgid="0x5179150c6657f95400000bc200000c58" relation="Relation6"/>
  <link port="GenIntConst#3.Trigger" portgid="0x5179142c6657f95400000ab500000c58" relation="Relation6"/>
  <link port="ConstStringGen#3.Trigger" portgid="0x5179142c6657f95400000ab100000c58" relation="Relation6"/>
  <link port="PulseGen#5.output" portgid="0x517915256657f95400000bd400000c58" relation="Relation5"/>
  <link port="ConstStringGen#4.Trigger" portgid="0x5179149a6657f95400000b9500000c58" relation="Relation5"/>
  <link port="GenIntConst#4.Trigger" portgid="0x5179149a6657f95400000b9900000c58" relation="Relation5"/>
  <link port="ServiceBasic#1.RejectDS" portgid="0x5178ef5d6657f954000001ff00000c58" relation="Relation2"/>
  <link port="TkText.input=1#4.input#1" portgid="0x5179156b6657f95400000c1c00000c58" relation="Relation2"/>
  <link port="ServiceBasic#1.OutArbitraryDS" portgid="0x5178ef5d6657f954000001fe00000c58" relation="Relation3"/>
  <link port="TkText.input=1#3.input#1" portgid="0x5179156b6657f95400000c1700000c58" relation="Relation3"/>
  <link port="ConstStringGen#4.StringOutput" portgid="0x5179149a6657f95400000b9600000c58" relation="Relation8"/>
  <link port="ConstStringGen#1.StringOutput" portgid="0x5179142c6657f95400000aa200000c58" relation="Relation8"/>
  <link port="ConstStringGen#3.StringOutput" portgid="0x5179142c6657f95400000ab200000c58" relation="Relation8"/>
  <link port="ServiceBasic#1.InArbitraryDS" portgid="0x5178ef5d6657f954000001fc00000c58" relation="Relation8"/>
  <link port="ConstStringGen#2.StringOutput" portgid="0x5179142c6657f95400000aaa00000c58" relation="Relation8"/>
  <link port="GenIntConst#4.Output" portgid="0x5179149a6657f95400000b9a00000c58" relation="Relation1"/>
  <link port="GenIntConst#2.Output" portgid="0x5179142c6657f95400000aae00000c58" relation="Relation1"/>
  <link port="GenIntConst#1.Output" portgid="0x5179142c6657f95400000aa600000c58" relation="Relation1"/>
  <link port="ServiceBasic#1.ServiceTime" portgid="0x5178ef5d6657f954000001fd00000c58" relation="Relation1"/>
  <link port="ServiceBasic#1.TimeDeadline" portgid="0x5178ef5d6657f9540000020000000c58" relation="Relation1"/>
  <link port="GenIntConst#3.Output" portgid="0x5179142c6657f95400000ab600000c58" relation="Relation1"/>
</model>

