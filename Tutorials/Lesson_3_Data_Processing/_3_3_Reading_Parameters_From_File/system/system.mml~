<?xml version='1.0' encoding='ISO-8859-1' ?>
<model gid="0x51629f566657f954000002dd00000c14" name="system" version="3.1.0" xmlns="http://www.mldesigner.com/mld" xmlns:svg="http://www.w3.org/2000/svg">
  <svg:svg height="100%" width="100%">
    <property class="String" name="Bounding visible" value="false"/>
  </svg:svg>
  <parameter attributes="A_CONSTANT" gid="0x3dcfc0f38b38f65100002459000071c0" name="GlobalSeed" scope="External" type="int" value="1234567890"/>
  <parameter attributes="A_CONSTANT" gid="0x3dcfc0f38b38f6510000245b000071c0" name="RunLength" scope="External" type="float" value="100"/>
  <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x3dcfc0f38b38f6510000245c000071c0" name="magnitude" scope="Internal" type="float" value="&lt;$MLD_USER/Tutorials/Lesson_4_Data_Processing/_4_3_Reading_Parameters_From_File/magnitude">
    <property class="String" name="Description" value="The value of the output particles generated."/>
  </parameter>
  <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x3dcfc0f38b38f6510000245d000071c0" name="interval" scope="Internal" type="float" value="&lt;$MLD_USER/Tutorials/Lesson_4_Data_Processing/_4_3_Reading_Parameters_From_File/interval">
    <property class="String" name="Description" value="The interval of events."/>
  </parameter>
  <property class="String" name="Logical Name" value="system"/>
  <property class="String" name="Version" value="$Revision: 1.1 $ $Date: 2013/04/26 09:14:12 $"/>
  <property class="String" name="Copyright" value="Mission Level Design GmbH"/>
  <property class="String" name="Author" value="Mission Level Design GmbH"/>
  <property class="String" name="hidden" value="no"/>
  <director class="DE" name="DE">
    <target class="default-DE">
      <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x3dcfc0f38b38f6510000245e000071c0" name="timeScale" scope="External" type="float" value="1.0">
        <property class="String" name="Description" value="Relative time scale for interface with another timed domain"/>
      </parameter>
      <parameter attributes="A_CONSTANT|A_SETTABLE" enumindices=",,,," enumlabels="Calender Queue Scheduler,Mutable Calendar Queue Scheduler,Resource Contention scheduler,Priority Free Scheduler,Priority scheduler" enumvalues="0,1,2,3,4" gid="0x3dcfc0f38b38f65100002460000071c0" name="usedScheduler" scope="External" type="enum" value="0">
        <property class="String" name="Description" value="Specifies used DE scheduler, default is Priority Free Scheduler.&#10;"/>
      </parameter>
    </target>
  </director>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-239,123"/>
      <property class="String" name="Text" value="Parameters can be read from a file. For this purpose,&#10;the character '&lt;' followed by the file path entered in&#10;the 'Value' field of the parameter."/>
      <property class="String" name="Alignment" value="bottomleft"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-227,66"/>
      <property class="String" name="Font" value="Helvetica:12::::"/>
      <property class="String" name="Text" value="Notes"/>
    </svg:svg>
  </label>
  <import name="_3_3_Reading_Parameters_From_File" url="../_3_3_Reading_Parameters_From_File.mml" urlgid="0x51629f566657f954000002df00000c14"/>
  <entity class="$MLD/MLD_Libraries/DE/Sources/Clock/Clock.mml" classgid="0x3dac05cc8b38f651000000a600003aad" gid="0x3dcfc0f88b38f651000024ff000071c0" name="Clock#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-204,-4"/>
      <property class="String" name="ZOrder" value="0"/>
      <property class="String" name="BackgroundColor" value="#ffffff"/>
    </svg:svg>
    <property class="String" name="Description" value="&#10;Generate events at regular intervals, starting at time zero.&#10; "/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68858b38f651000083a100007914" gid="0x3dcfc0f88b38f65100002500000071c0" name="interval" scope="External" type="float" value="$interval">
      <property class="String" name="Description" value="The interval of events."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68858b38f651000083a200007914" gid="0x3dcfc0f88b38f65100002501000071c0" name="magnitude" scope="External" type="float" value="$magnitude">
      <property class="String" name="Description" value="The value of the output particles generated."/>
    </parameter>
    <port class="float" formalgid="0x3dcf68858b38f651000083a300007914" gid="0x3dcfc0f88b38f65100002502000071c0" name="output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="28,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="32,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/Sinks/Xgraph/Xgraph.mml" classgid="0x3dac0a218b38f651000000f700003aad" gid="0x3dcfc0f88b38f65100002503000071c0" name="Xgraph#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-40,-4"/>
      <property class="String" name="ZOrder" value="0"/>
      <property class="String" name="BackgroundColor" value="#ffffff"/>
    </svg:svg>
    <property class="String" name="Description" value="Generate a plot with the xgraph program."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68518b38f65100007c1b00007914" gid="0x3dcfc0f88b38f65100002504000071c0" name="title" scope="External" type="string" value="">
      <property class="String" name="Description" value="Graph title."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68518b38f65100007c1c00007914" gid="0x3dcfc0f88b38f65100002505000071c0" name="saveFile" scope="External" type="filename" value="">
      <property class="String" name="Description" value="File to save xgraph input."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68518b38f65100007c1d00007914" gid="0x3dcfc0f88b38f65100002506000071c0" name="options" scope="External" type="string" value="-bb -tk -P =800x400">
      <property class="String" name="Description" value="Command line options for xgraph."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" enumindices=",," enumlabels="None,Iterations,Paramsets" enumvalues="XG_CUM_NONE,XG_CUM_ITERS,XG_CUM_PMSET" formalgid="0x3dcf68518b38f65100007c1e00007914" gid="0x3dcfc0f88b38f65100002507000071c0" name="Cumulation" scope="External" type="enum" value="XG_CUM_NONE">
      <property class="String" name="Description" value="Specifies the cumulation level for the graph display:&#10;None - a graph display for each iteration;&#10;Iterations - a graph display for all iterations of a parameter set;&#10;Paramsets - a single graph display for the whole simulation."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x4a79702242787b4f0000005f00005fa1" gid="0x4ed3a39e8f309d76000005360000258b" name="Enabled" scope="External" type="int" value="YES">
      <property class="String" name="Description" value="Enable or disable collecting/displaying data and writing files."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf68518b38f65100007c1f00007914" gid="0x3dcfc0f88b38f65100002508000071c0" name="input" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-32,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-36,0"/>
      </svg:svg>
    </port>
  </entity>
  <relation name="Relation1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-172,-4 -76,-4"/>
      <property class="String" name="Edges" value="0,1"/>
      <property class="String" name="Color" value="#000000"/>
    </svg:svg>
  </relation>
  <link port="Clock#1.output" portgid="0x3dcfc0f88b38f65100002502000071c0" relation="Relation1"/>
  <link port="Xgraph#1.input" portgid="0x3dcfc0f88b38f65100002508000071c0" relation="Relation1"/>
</model>

