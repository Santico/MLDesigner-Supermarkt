// This file was generated by MLDesigner version 3.1.0


// importing SystemDS library for data structure definition
#include "$MLD/lib/system/SystemDS.cpp"

// importing libraries for data structure definition
#include "../_3_3_Reading_Parameters_From_File.cpp"

setDomain("DE");



defSystem("system");
  setDomain("DE");
  // define the target and set the target parameters
  setTarget("default-DE");
  setTargetParam("timeScale", "1.0");
  setTargetParam("usedScheduler", "0");

  // definition of model parameters
  newParam("GlobalSeed", "int", "1234567890", false);
  newParam("RunLength", "float", "100", false);
  newParam("magnitude", "float", "<$MLD_USER/Tutorials/Lesson_4_Data_Processing/_4_3_Reading_Parameters_From_File/magnitude", true);
  newParam("interval", "float", "<$MLD_USER/Tutorials/Lesson_4_Data_Processing/_4_3_Reading_Parameters_From_File/interval", true);

  // definition of model memories

  // definition of model events

  // definition of model resources

  // definition of instances and their properties
  createInstance("Clock#1", "Clock", "", false);
  setParam("Clock#1",  "interval", "$interval");
  setParam("Clock#1",  "magnitude", "$magnitude");
  createInstance("Xgraph#1", "Xgraph", "", false);
  setParam("Xgraph#1",  "title", "");
  setParam("Xgraph#1",  "saveFile", "");
  setParam("Xgraph#1",  "options", "-bb -tk -P =800x400");
  setParam("Xgraph#1",  "Cumulation", "0");
  setParam("Xgraph#1",  "Enabled", "YES");

  // define the connections
  connect("Clock#1", "output", "Xgraph#1", "input", "");

  // create sources and sinks for autoterminated ports


