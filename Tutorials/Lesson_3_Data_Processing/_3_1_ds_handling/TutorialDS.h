/* This file was automaticaly generated. */
#ifndef _3_1_ds_handling_TutorialDS_h
#define _3_1_ds_handling_TutorialDS_h

#include "kernel/DsHandler.h"
#include "kernel/Root.h"
#include "kernel/BaseTypes.h"

/* Data structure class definition. */
/* The namespace is generated from the library name. */
namespace _3_1_ds_handling
{
class TutorialDSR : public RootR
{
  public:
#define TutorialDS_INITS \
  ,Number(mValue.getFieldRef(0))

    //constructors
    DEFINEREFCONSTRUCTORS(TutorialDS, Root, TutorialDS_INITS)

    IntegerR Number;

  /* Protected section */
  private:
    DEFINENEWVALUE("_3_1_ds_handling:Root.TutorialDS");

};// class definition
} // namespace

#endif
