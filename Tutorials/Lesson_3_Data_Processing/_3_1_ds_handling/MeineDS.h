/* This file was automaticaly generated. */
#ifndef _3_1_ds_handling_MeineDS_h
#define _3_1_ds_handling_MeineDS_h

#include "kernel/DsHandler.h"
#include "kernel/Root.h"
#include "kernel/BaseTypes.h"

/* Data structure class definition. */
/* The namespace is generated from the library name. */
namespace _3_1_ds_handling
{
class MeineDSR : public RootR
{
  public:
#define MeineDS_INITS \
  ,x(mValue.getFieldRef(0))\
  ,baum(mValue.getFieldRef(1))

    //constructors
    DEFINEREFCONSTRUCTORS(MeineDS, Root, MeineDS_INITS)

    FloatR x;
    IntegerR baum;

  /* Protected section */
  private:
    DEFINENEWVALUE("_3_1_ds_handling:Root.MeineDS");

};// class definition
} // namespace

#endif
