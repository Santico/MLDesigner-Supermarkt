<?xml version='1.0' encoding='ISO-8859-1' ?>
<model gid="0x51629f566657f954000002d100000c14" name="system" version="3.1.0" xmlns="http://www.mldesigner.com/mld" xmlns:svg="http://www.w3.org/2000/svg">
  <svg:svg height="100%" width="100%">
    <property class="String" name="Bounding visible" value="false"/>
  </svg:svg>
  <property class="String" name="Description" value="Reads a matrix of vectors from a file."/>
  <parameter attributes="A_CONSTANT" gid="0x3dcfc0fa8b38f6510000254e000071c0" name="GlobalSeed" scope="External" type="int" value="1234567890"/>
  <parameter attributes="A_CONSTANT" gid="0x3dcfc0fa8b38f65100002550000071c0" name="RunLength" scope="External" type="float" value="100"/>
  <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x516bea536657f954000001f500000f98" name="FloatMatrixFile" scope="Internal" type="filename" value="$MLD_USER/Tutorials/Lesson_4_Data_Processing/_4_4_matrix_operations/Matrix">
    <property class="String" name="Description" value="The name of the file containing the matrix elements."/>
  </parameter>
  <property class="String" name="Logical Name" value="system"/>
  <property class="String" name="Version" value="$Revision: 1.1 $ $Date: 2013/04/26 09:14:12 $"/>
  <property class="String" name="Copyright" value="Mission Level Design GmbH"/>
  <property class="String" name="Author" value="Mission Level Design GmbH"/>
  <property class="String" name="hidden" value="no"/>
  <director class="DE" name="DE">
    <target class="default-DE">
      <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x3dcfc0fa8b38f65100002551000071c0" name="timeScale" scope="External" type="float" value="1.0">
        <property class="String" name="Description" value="Relative time scale for interface with another timed domain"/>
      </parameter>
      <parameter attributes="A_CONSTANT|A_SETTABLE" enumindices=",,,," enumlabels="Calender Queue Scheduler,Mutable Calendar Queue Scheduler,Resource Contention scheduler,Priority Free Scheduler,Priority scheduler" enumvalues="0,1,2,3,4" gid="0x3dcfc0fa8b38f65100002553000071c0" name="usedScheduler" scope="External" type="enum" value="3">
        <property class="String" name="Description" value="Specifies used DE scheduler, default is Priority Free Scheduler.&#10;"/>
      </parameter>
    </target>
  </director>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-355,118"/>
      <property class="String" name="Text" value="This system shows the handling with matrices.&#10;First a matrix of float values will be read from a file. Open the file of the 'ReadFileFMatrix' primitive by right click the  parameter 'FloatMatrixFile'&#10;and select 'open'.&#10;The primitive 'AccessElementFMatrix' will return the Element of the matrix at  the position specified by the 'RowIndex' and 'ColumnIndex' inputs.&#10;The indices start at zero.&#10;The size of the matrix will be outputted by the 'DimensionFMatrix'  primitive."/>
      <property class="String" name="Alignment" value="topleft"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-323,94"/>
      <property class="String" name="Font" value="Helvetica:12::::"/>
      <property class="String" name="Text" value="Description"/>
    </svg:svg>
  </label>
  <entity class="$MLD/MLD_Libraries/DE/MatrixOperations/FloatMatrix/ReadFileFMatrix/ReadFileFMatrix.mml" classgid="0x3dac0b568b38f651000003dc00003aad" gid="0x3dcfc0fa8b38f65100002554000071c0" name="ReadFileFMatrix#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-204,-12"/>
      <property class="String" name="ZOrder" value="0"/>
      <property class="String" name="BackgroundColor" value="#ffffff"/>
      <property class="String" name="Label" value="ReadFileFMatrix"/>
    </svg:svg>
    <property class="String" name="Description" value="Creates a FloatMatrix data structure, reading the values from an input file."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68ad8b38f6510000870400007914" gid="0x3dcfc0fa8b38f65100002555000071c0" name="FloatMatrixFile" scope="External" type="filename" value="$MLD_USER/Tutorials/Lesson_3_Data_Processing/_3_4_matrix_operations/Matrix">
      <property class="String" name="Description" value="The name of the file containing the matrix elements."/>
    </parameter>
    <port class="int" formalgid="0x3dcf68ad8b38f6510000870500007914" gid="0x3dcfc0fa8b38f65100002556000071c0" name="Rows" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-56,-4"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-56,-4"/>
        <property class="String" name="Show Instance Label" value="false"/>
      </svg:svg>
      <property class="String" name="Description" value="The number of rows in the matrix."/>
    </port>
    <port class="int" formalgid="0x3dcf68ad8b38f6510000870600007914" gid="0x3dcfc0fa8b38f65100002557000071c0" name="Columns" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-56,8"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-56,8"/>
        <property class="String" name="Show Instance Label" value="false"/>
      </svg:svg>
      <property class="String" name="Description" value="The number of columns in the matrix."/>
    </port>
    <port class="datastruct" datastruct="SystemDS:Root.BaseMatrix.FloatMatrix" datastructgid="0x3d9b0d861faba0e60000003800004d2a" formalgid="0x3dcf68ad8b38f6510000870700007914" gid="0x3dcfc0fa8b38f65100002558000071c0" name="FloatMatrix" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="56,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="56,0"/>
        <property class="String" name="Show Instance Label" value="true"/>
      </svg:svg>
      <property class="String" name="Description" value="The FloatMatrix read from the file."/>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/Control/Init/Init.mml" classgid="0x3dac05d08b38f651000000b500003aad" gid="0x3dcfc0fb8b38f65100002563000071c0" name="Init#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-400,-8"/>
      <property class="String" name="BackgroundColor" value="#e6e6e6"/>
      <property class="String" name="ZOrder" value="0"/>
      <property class="String" name="Label" value="Init Event"/>
    </svg:svg>
    <property class="String" name="Description" value="Places a trigger when an iteration starts."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68208b38f651000070d600007914" gid="0x3dcfc0fb8b38f65100002564000071c0" name="RelativeOrder" scope="External" type="int" value="0">
      <property class="String" name="Description" value="This state sets priority for firing. It can take values between -MAX_INT (lowest priority) and +MAX_INT (highest priority)."/>
    </parameter>
    <port class="int" formalgid="0x3dcf68208b38f651000070d800007914" gid="0x3dcfc0fb8b38f65100002565000071c0" name="out" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="88,40"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="28,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/NumberGenerators/GenIntConst/GenIntConst.mml" classgid="0x3daa70498b38f6510000017a00004dd6" gid="0x3dcfc0fb8b38f65100002566000071c0" name="GenIntConst#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-316,-28"/>
      <property class="String" name="ZOrder" value="0"/>
      <property class="String" name="BackgroundColor" value="#e6e6e6"/>
      <property class="String" name="Label" value="2Rows"/>
    </svg:svg>
    <property class="String" name="Description" value="Outputs a constant integer value"/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf681e8b38f6510000709e00007914" gid="0x3dcfc0fb8b38f65100002567000071c0" name="Value" scope="External" type="int" value="2">
      <property class="String" name="Description" value="The constant value."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf681e8b38f6510000709f00007914" gid="0x3dcfc0fb8b38f65100002568000071c0" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-25,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-32,0"/>
      </svg:svg>
    </port>
    <port class="int" formalgid="0x3dcf681e8b38f651000070a000007914" gid="0x3dcfc0fb8b38f65100002569000071c0" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="25,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="32,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/NumberGenerators/GenIntConst/GenIntConst.mml" classgid="0x3daa70498b38f6510000017a00004dd6" gid="0x3dcfc0fb8b38f6510000256a000071c0" name="GenIntConst#2">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-312,12"/>
      <property class="String" name="ZOrder" value="0"/>
      <property class="String" name="BackgroundColor" value="#e6e6e6"/>
      <property class="String" name="Label" value="3Columns"/>
    </svg:svg>
    <property class="String" name="Description" value="Outputs a constant integer value"/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf681e8b38f6510000709e00007914" gid="0x3dcfc0fb8b38f6510000256b000071c0" name="Value" scope="External" type="int" value="3">
      <property class="String" name="Description" value="The constant value."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf681e8b38f6510000709f00007914" gid="0x3dcfc0fb8b38f6510000256c000071c0" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-25,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-36,0"/>
      </svg:svg>
    </port>
    <port class="int" formalgid="0x3dcf681e8b38f651000070a000007914" gid="0x3dcfc0fb8b38f6510000256d000071c0" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="25,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="36,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/MatrixOperations/FloatMatrix/AccessElementFMatrix/AccessElementFMatrix.mml" classgid="0x3dac139e8b38f651000005f500003aad" gid="0x516be8296657f9540000047c000012c8" name="AccessElementFMatrix#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="72,0"/>
      <property class="String" name="Label" value="AccessElementFMatrix"/>
    </svg:svg>
    <property class="String" name="Description" value="This module is used to access an element in a FloatMatrix data structure. The position of the element is specified by the 'RowIndex' and 'ColumnIndex' inputs. Be careful with the values of this inputs, cause there is no range checking in the module."/>
    <port class="int" formalgid="0x3eb23b6a8717fa7e00000023000020f8" gid="0x516be8296657f9540000047e000012c8" name="RowIndex" type="input">
      <svg:svg height="100%" width="100%">
        <property class="String" name="Position" value="-44,-12"/>
        <property class="String" name="ConnectPoint" value="-34,-12"/>
        <property class="String" name="PortAlign" value="left"/>
      </svg:svg>
      <property class="String" name="Description" value="The row of the element."/>
    </port>
    <port class="datastruct" datastruct="SystemDS:Root.BaseMatrix.FloatMatrix" datastructgid="0x3d9b0d861faba0e60000003800004d2a" formalgid="0x3eb23b6d8717fa7e00000024000020f8" gid="0x516be8296657f9540000047f000012c8" name="MatrixIn" type="input">
      <svg:svg height="100%" width="100%">
        <property class="String" name="Position" value="-44,-56"/>
        <property class="String" name="ConnectPoint" value="-34,-56"/>
        <property class="String" name="PortAlign" value="left"/>
      </svg:svg>
      <property class="String" name="Description" value="The FloatMatrix data structure for which the element will be accessed."/>
    </port>
    <port class="int" formalgid="0x3eb23b728717fa7e00000025000020f8" gid="0x516be8296657f95400000480000012c8" name="ColumnIndex" type="input">
      <svg:svg height="100%" width="100%">
        <property class="String" name="Position" value="-44,28"/>
        <property class="String" name="ConnectPoint" value="-34,28"/>
        <property class="String" name="PortAlign" value="left"/>
      </svg:svg>
      <property class="String" name="Description" value="The column of the element."/>
    </port>
    <port class="datastruct" datastruct="SystemDS:Root.BaseMatrix.FloatMatrix" datastructgid="0x3d9b0d861faba0e60000003800004d2a" formalgid="0x3eb23b788717fa7e00000026000020f8" gid="0x516be8296657f95400000481000012c8" name="MatrixOut" type="output">
      <svg:svg height="100%" width="100%">
        <property class="String" name="Position" value="44,-36"/>
        <property class="String" name="ConnectPoint" value="34,-36"/>
        <property class="String" name="PortAlign" value="right"/>
      </svg:svg>
      <property class="String" name="Description" value="The FloatMatrix data structure given by the &quot;MatrixIn&quot; input."/>
    </port>
    <port class="float" formalgid="0x3eb23b798717fa7e00000027000020f8" gid="0x516be8296657f95400000482000012c8" name="Element" type="output">
      <svg:svg height="100%" width="100%">
        <property class="String" name="Position" value="44,12"/>
        <property class="String" name="ConnectPoint" value="34,12"/>
        <property class="String" name="PortAlign" value="right"/>
      </svg:svg>
      <property class="String" name="Description" value="The value of the element."/>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/MatrixOperations/FloatMatrix/DimensionsFMatrix/DimensionsFMatrix.mml" classgid="0x3dac13a08b38f651000005f700003aad" gid="0x516be8386657f9540000048b000012c8" name="DimensionsFMatrix#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="64,-52"/>
      <property class="String" name="Label" value="DimensionsFMatrix"/>
    </svg:svg>
    <property class="String" name="Description" value="This module returns the size of the row and column dimensions of the FloatMatrix data structure given by the 'Matrix' input."/>
    <port class="datastruct" datastruct="SystemDS:Root.BaseMatrix.FloatMatrix" datastructgid="0x3d9b0d861faba0e60000003800004d2a" formalgid="0x3eb260688717fa7e00000058000020f8" gid="0x516be8386657f9540000048d000012c8" name="Matrix" type="input">
      <svg:svg height="100%" width="100%">
        <property class="String" name="Position" value="-44,12"/>
        <property class="String" name="ConnectPoint" value="-34,12"/>
        <property class="String" name="PortAlign" value="left"/>
      </svg:svg>
      <property class="String" name="Description" value="The FloatMatrix data structure."/>
    </port>
    <port class="int" formalgid="0x3eb2606c8717fa7e00000059000020f8" gid="0x516be8386657f9540000048e000012c8" name="Rows" type="output">
      <svg:svg height="100%" width="100%">
        <property class="String" name="Position" value="44,-8"/>
        <property class="String" name="ConnectPoint" value="34,-8"/>
        <property class="String" name="PortAlign" value="right"/>
      </svg:svg>
      <property class="String" name="Description" value="The number of rows in the matrix."/>
    </port>
    <port class="int" formalgid="0x3eb260708717fa7e0000005a000020f8" gid="0x516be8386657f9540000048f000012c8" name="Columns" type="output">
      <svg:svg height="100%" width="100%">
        <property class="String" name="Position" value="44,36"/>
        <property class="String" name="ConnectPoint" value="34,36"/>
        <property class="String" name="PortAlign" value="right"/>
      </svg:svg>
      <property class="String" name="Description" value="The number of columns in the matrix."/>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/TclTk/TkText/TkText.input=4.mml" classgid="0x3dac10378b38f651000004ad00003aad" gid="0x516be8e16657f9540000015900000f98" name="TkText.input=4#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="220,-12"/>
      <property class="String" name="ZOrder" value="0"/>
      <property class="String" name="BackgroundColor" value="#e6e6e6"/>
    </svg:svg>
    <property class="String" name="Description" value="Display the values of the inputs in a separate window,&#10;keeping a specified number of past values in view.&#10;The print method of the input particles is used, so any data&#10;type can be handled."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf689b8b38f651000085cd00007914" gid="0x516be8e16657f9540000015600000f98" name="label" scope="External" type="string" value="Data Structure with inserted Field">
      <property class="String" name="Description" value="A label to put on the display"/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf689b8b38f651000085cf00007914" gid="0x516be8e16657f9540000015700000f98" name="wait_between_outputs" scope="External" type="int" value="NO">
      <property class="String" name="Description" value="Specify whether to wait for user input between output values"/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf689b8b38f651000085cb00007914" gid="0x516be8e16657f9540000015800000f98" name="number_of_past_values" scope="External" type="string" value="10">
      <property class="String" name="Description" value="Specify how many past values you would like saved"/>
    </parameter>
    <port derivegid="0x3dcf68978b38f6510000855100007914" formalgid="0x3df6136c20169f5b0001c11600001cb5" gid="0x516be8e16657f9540000015a00000f98" name="input#1">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-56,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="10,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Any number of inputs to feed to Tcl"/>
    </port>
    <port derivegid="0x3dcf68978b38f6510000855100007914" formalgid="0x3df6136c20169f5b0001c13300001cb5" gid="0x516be8e16657f9540000015b00000f98" name="input#2">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-56,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="10,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Any number of inputs to feed to Tcl"/>
    </port>
    <port derivegid="0x3dcf68978b38f6510000855100007914" formalgid="0x3df6136c20169f5b0001c13400001cb5" gid="0x516be8e16657f9540000015c00000f98" name="input#3">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-56,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="10,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Any number of inputs to feed to Tcl"/>
    </port>
    <port derivegid="0x3dcf68978b38f6510000855100007914" formalgid="0x3df6136c20169f5b0001c13500001cb5" gid="0x516be8e16657f9540000015d00000f98" name="input#4">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-56,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="10,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Any number of inputs to feed to Tcl"/>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/NumberGenerators/GenIntConst/GenIntConst.mml" classgid="0x3daa70498b38f6510000017a00004dd6" gid="0x516be9296657f954000001a200000f98" name="GenIntConst#3">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-80,16"/>
      <property class="String" name="ZOrder" value="0"/>
      <property class="String" name="BackgroundColor" value="#e6e6e6"/>
      <property class="String" name="Label" value="1st Row (0)"/>
    </svg:svg>
    <property class="String" name="Description" value="Outputs a constant integer value"/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf681e8b38f6510000709e00007914" gid="0x516be9296657f954000001a100000f98" name="Value" scope="External" type="int" value="0">
      <property class="String" name="Description" value="The constant value."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf681e8b38f6510000709f00007914" gid="0x516be9296657f954000001a300000f98" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-25,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-32,0"/>
      </svg:svg>
    </port>
    <port class="int" formalgid="0x3dcf681e8b38f651000070a000007914" gid="0x516be9296657f954000001a400000f98" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="25,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="32,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/NumberGenerators/GenIntConst/GenIntConst.mml" classgid="0x3daa70498b38f6510000017a00004dd6" gid="0x516be9296657f954000001a600000f98" name="GenIntConst#4">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-76,56"/>
      <property class="String" name="ZOrder" value="0"/>
      <property class="String" name="BackgroundColor" value="#e6e6e6"/>
      <property class="String" name="Label" value="2nd Column (1)"/>
    </svg:svg>
    <property class="String" name="Description" value="Outputs a constant integer value"/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf681e8b38f6510000709e00007914" gid="0x516be9296657f954000001a500000f98" name="Value" scope="External" type="int" value="1">
      <property class="String" name="Description" value="The constant value."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf681e8b38f6510000709f00007914" gid="0x516be9296657f954000001a700000f98" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-25,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-36,0"/>
      </svg:svg>
    </port>
    <port class="int" formalgid="0x3dcf681e8b38f651000070a000007914" gid="0x516be9296657f954000001a800000f98" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="25,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="36,0"/>
      </svg:svg>
    </port>
  </entity>
  <relation name="Relation1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-364,-8 -348,-28 -348,12 -356,-8 -356,12 -356,-28"/>
      <property class="String" name="Edges" value="0,3 1,5 2,4 3,5 3,4"/>
      <property class="String" name="Color" value="#000000"/>
    </svg:svg>
  </relation>
  <relation name="Relation4">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-276,12 -256,-4 -268,-4 -268,12"/>
      <property class="String" name="Edges" value="0,3 1,2 2,3"/>
      <property class="String" name="Color" value="#000000"/>
    </svg:svg>
  </relation>
  <relation name="Relation3">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-28,56 4,12 -16,56 -16,12"/>
      <property class="String" name="Edges" value="0,2 1,3 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation6">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-36,16 4,0 -24,16 -24,0"/>
      <property class="String" name="Edges" value="0,2 1,3 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation7">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="140,-4 168,-4"/>
      <property class="String" name="Edges" value="0,1"/>
    </svg:svg>
  </relation>
  <relation name="Relation8">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="140,8 168,8"/>
      <property class="String" name="Edges" value="0,1"/>
    </svg:svg>
  </relation>
  <relation name="Relation9">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="124,-56 168,-28 156,-28 156,-56"/>
      <property class="String" name="Edges" value="0,3 1,2 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation10">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="124,-44 168,-16 148,-16 148,-44"/>
      <property class="String" name="Edges" value="0,3 1,2 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation5">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-152,-12 4,-52 4,-12 -124,16 -124,56 -132,16 -44,-52 -132,56 -44,-12 -132,-12"/>
      <property class="String" name="Edges" value="0,9 1,6 2,8 3,5 4,7 5,9 5,7 6,8 8,9"/>
    </svg:svg>
  </relation>
  <relation name="Relation2">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-284,-28 -256,-16 -268,-16 -268,-28"/>
      <property class="String" name="Edges" value="0,3 1,2 2,3"/>
      <property class="String" name="Color" value="#000000"/>
    </svg:svg>
  </relation>
  <link port="Init#1.out" portgid="0x3dcfc0fb8b38f65100002565000071c0" relation="Relation1"/>
  <link port="GenIntConst#1.Trigger" portgid="0x3dcfc0fb8b38f65100002568000071c0" relation="Relation1"/>
  <link port="GenIntConst#2.Trigger" portgid="0x3dcfc0fb8b38f6510000256c000071c0" relation="Relation1"/>
  <link port="GenIntConst#2.Output" portgid="0x3dcfc0fb8b38f6510000256d000071c0" relation="Relation4"/>
  <link port="ReadFileFMatrix#1.Columns" portgid="0x3dcfc0fa8b38f65100002557000071c0" relation="Relation4"/>
  <link port="AccessElementFMatrix#1.ColumnIndex" portgid="0x516be8296657f95400000480000012c8" relation="Relation3"/>
  <link port="GenIntConst#4.Output" portgid="0x516be9296657f954000001a800000f98" relation="Relation3"/>
  <link port="GenIntConst#3.Output" portgid="0x516be9296657f954000001a400000f98" relation="Relation6"/>
  <link port="AccessElementFMatrix#1.RowIndex" portgid="0x516be8296657f9540000047e000012c8" relation="Relation6"/>
  <link port="AccessElementFMatrix#1.MatrixOut" portgid="0x516be8296657f95400000481000012c8" relation="Relation7"/>
  <link port="TkText.input=4#1.input#3" portgid="0x516be8e16657f9540000015c00000f98" relation="Relation7"/>
  <link port="AccessElementFMatrix#1.Element" portgid="0x516be8296657f95400000482000012c8" relation="Relation8"/>
  <link port="TkText.input=4#1.input#4" portgid="0x516be8e16657f9540000015d00000f98" relation="Relation8"/>
  <link port="DimensionsFMatrix#1.Rows" portgid="0x516be8386657f9540000048e000012c8" relation="Relation9"/>
  <link port="TkText.input=4#1.input#1" portgid="0x516be8e16657f9540000015a00000f98" relation="Relation9"/>
  <link port="DimensionsFMatrix#1.Columns" portgid="0x516be8386657f9540000048f000012c8" relation="Relation10"/>
  <link port="TkText.input=4#1.input#2" portgid="0x516be8e16657f9540000015b00000f98" relation="Relation10"/>
  <link port="ReadFileFMatrix#1.FloatMatrix" portgid="0x3dcfc0fa8b38f65100002558000071c0" relation="Relation5"/>
  <link port="DimensionsFMatrix#1.Matrix" portgid="0x516be8386657f9540000048d000012c8" relation="Relation5"/>
  <link port="AccessElementFMatrix#1.MatrixIn" portgid="0x516be8296657f9540000047f000012c8" relation="Relation5"/>
  <link port="GenIntConst#3.Trigger" portgid="0x516be9296657f954000001a300000f98" relation="Relation5"/>
  <link port="GenIntConst#4.Trigger" portgid="0x516be9296657f954000001a700000f98" relation="Relation5"/>
  <link port="GenIntConst#1.Output" portgid="0x3dcfc0fb8b38f65100002569000071c0" relation="Relation2"/>
  <link port="ReadFileFMatrix#1.Rows" portgid="0x3dcfc0fa8b38f65100002556000071c0" relation="Relation2"/>
</model>

