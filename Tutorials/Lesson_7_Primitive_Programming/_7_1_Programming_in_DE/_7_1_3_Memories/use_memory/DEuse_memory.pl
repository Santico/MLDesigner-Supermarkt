defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { use_memory }
  domain      { DE }
  author      { fb }
  version     { 0.0 09/21/2012 }
  copyright   { Mission Level Design GmbH }


  input
  {
    name { trigger }
    type { anytype }
  }

  output
  {
    name { old_value }
    type { int }
  }

  output
  {
    name { new_value }
    type { int }
  }

  defmemory
  {
    name    { IntegerMemory }
    scope   { External }
    type    { Root.Integer }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  go
  { 
    // Memories can be used much like variables. New values are assigned with '=' to the memory, which can be accessed directly.
    old_value.put(arrivalTime) << (int)IntegerMemory;
    IntegerMemory = (int)IntegerMemory +1;
    new_value.put(arrivalTime) << (int)IntegerMemory;
  }

}
