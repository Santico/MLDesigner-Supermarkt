defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { Add_Ext }
  domain      { DE }
  author      { Sven Kein }
  version     { 0.0 04/17/2013 }

  input
  {
    name { Input1 }
    type { int }
  }

  input
  {
    name { Input2 }
    type { int }
  }

  output
  {
    name { Output1 }
    type { int }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  code
  {

    // Invoking the function from the linked library with the definition of the arguments

extern int externalLibraryAdd(int, int);
  }

  constructor
  {
    setAndInputCondition();
  }

  go
  {
    int a = (int)Input1.get();
    int b = (int)Input2.get();
    // Using the function from the external library as defined in the "code" segment
    int c = externalLibraryAdd( a, b );
    Output1.put(arrivalTime) << c;
  }

}
