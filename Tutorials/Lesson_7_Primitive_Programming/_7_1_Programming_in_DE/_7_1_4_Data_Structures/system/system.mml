<?xml version='1.0' encoding='ISO-8859-1' ?>
<model gid="0x51629f7a6657f9540000031a00000c14" name="system" version="3.1.0" xmlns="http://www.mldesigner.com/mld" xmlns:svg="http://www.w3.org/2000/svg">
  <svg:svg height="100%" width="100%">
    <property class="String" name="Bounding visible" value="false"/>
  </svg:svg>
  <parameter attributes="A_CONSTANT" gid="0x505c6429af68b1c400000243000009a8" name="GlobalSeed" scope="External" type="int" value="1234567890"/>
  <parameter attributes="A_CONSTANT" gid="0x505c6429af68b1c400000244000009a8" name="RunLength" scope="External" type="float" value="1">
    <property class="String" name="Description" value="Determines the number of cycles for non-timed domains or the time to run in seconds for timed domains. The maximum value is MAX_INT, i.e., 2^15-1. Value -1 can be used to define a pseudo-infinite runlength for domains that support the EndCondition feature by primitives."/>
  </parameter>
  <property class="String" name="Logical Name" value="system"/>
  <property class="String" name="Version" value="0.0 09/21/2012"/>
  <property class="String" name="Copyright" value="Mission Level Design GmbH"/>
  <property class="String" name="Author" value="Mission Level Design GmbH"/>
  <property class="String" name="hidden" value="no"/>
  <director class="DE" name="DE">
    <target class="default-DE">
      <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x505c6429af68b1c400000245000009a8" name="timeScale" scope="External" type="float" value="1.0">
        <property class="String" name="Description" value="Relative time scale for interface with another timed domain"/>
      </parameter>
      <parameter attributes="A_CONSTANT|A_SETTABLE" enumindices="" enumlabels="Calendar Queue Scheduler,Mutable Calendar Queue Scheduler,Priority Free Scheduler,Priority scheduler,Resource Contention scheduler,Simple DE scheduler" enumvalues="0,1,3,4,2,5" gid="0x505c6429af68b1c400000246000009a8" name="usedScheduler" scope="External" type="enum" value="3">
        <property class="String" name="Description" value="Specifies used DE scheduler, default is Priority Free Scheduler.&#10;"/>
      </parameter>
    </target>
  </director>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-211,46"/>
      <property class="String" name="Font" value="Helvetica:12::::"/>
      <property class="String" name="Text" value="Description"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-239,86"/>
      <property class="String" name="Text" value="The primitve 'DS_handling' contains a example for using data structures..&#10;&#10;The explanation is included in the source code."/>
      <property class="String" name="Alignment" value="centerleft"/>
    </svg:svg>
  </label>
  <import name="_7_1_4_Data_Structures" url="../_7_1_4_Data_Structures.mml" urlgid="0x51629f7a6657f9540000031900000c14"/>
  <entity class="$MLD/MLD_Libraries/DE/Sources/Clock/Clock.mml" classgid="0x3dac05cc8b38f651000000a600003aad" gid="0x505c656baf68b1c400000254000009a8" name="Clock#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-176,-12"/>
    </svg:svg>
    <property class="String" name="Description" value="&#10;Generate events at regular intervals, starting at time zero.&#10; "/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68858b38f651000083a100007914" gid="0x505c656baf68b1c400000256000009a8" name="interval" scope="External" type="float" value="1.0">
      <property class="String" name="Description" value="The interval of events."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68858b38f651000083a200007914" gid="0x505c656baf68b1c400000257000009a8" name="magnitude" scope="External" type="float" value="1.0">
      <property class="String" name="Description" value="The value of the output particles generated."/>
    </parameter>
    <port class="float" formalgid="0x3dcf68858b38f651000083a300007914" gid="0x505c656baf68b1c400000255000009a8" name="output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="24,-36"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-10,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/DSHandling/PrintDS/PrintDS.mml" classgid="0x3dac0b568b38f651000003de00003aad" gid="0x505c70f7af68b1c40000034c000009a8" name="PrintDS#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="16,-12"/>
    </svg:svg>
    <property class="String" name="Description" value="Prints the fields and values of a data structure."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf69558b38f651000095bf00007914" gid="0x505c70f7af68b1c40000034f000009a8" name="Title" scope="External" type="string" value=""/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf69558b38f651000095c000007914" gid="0x505c70f7af68b1c400000350000009a8" name="FileName" scope="External" type="filename" value="&lt;stdout&gt;"/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf69558b38f651000095c100007914" gid="0x505c70f7af68b1c400000351000009a8" name="Binary" scope="External" type="boolean" value="FALSE"/>
    <port class="datastruct" datastruct="Root" datastructgid="0xffffffffffffffff0000000100000001" formalgid="0x3dcf69558b38f651000095c200007914" gid="0x505c70f7af68b1c40000034d000009a8" name="InDS" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,-24"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,-24"/>
      </svg:svg>
    </port>
    <port class="datastruct" datastruct="Root" datastructgid="0xffffffffffffffff0000000100000001" formalgid="0x3dcf69558b38f651000095c300007914" gid="0x505c70f7af68b1c40000034e000009a8" name="OutDS" terminated="true" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="44,-24"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,-24"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="../DS_handling/DS_handling.mml" classgid="0x51629f7a6657f9540000031800000c14" gid="0x505c7103af68b1c400000358000009a8" name="DS_handling#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-76,-12"/>
    </svg:svg>
    <port class="anytype" formalgid="0x505c6988af68b1c40000028a000009a8" gid="0x505c7103af68b1c400000359000009a8" name="trigger" type="input">
      <svg:svg height="100%" width="100%">
        <property class="String" name="Position" value="-56,16"/>
      </svg:svg>
    </port>
    <port class="datastruct" datastruct="Root" datastructgid="0xffffffffffffffff0000000100000001" formalgid="0x505c698faf68b1c40000028b000009a8" gid="0x505c7103af68b1c40000035a000009a8" name="dsOut" type="output">
      <svg:svg height="100%" width="100%">
        <property class="String" name="Position" value="56,16"/>
        <property class="String" name="PortAlign" value="right"/>
      </svg:svg>
    </port>
  </entity>
  <relation name="Relation1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-144,-12 -128,-12"/>
      <property class="String" name="Edges" value="0,1"/>
    </svg:svg>
  </relation>
  <relation name="Relation2">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-24,-12 -20,-12"/>
      <property class="String" name="Edges" value="0,1"/>
    </svg:svg>
  </relation>
  <link port="DS_handling#1.trigger" portgid="0x505c7103af68b1c400000359000009a8" relation="Relation1"/>
  <link port="Clock#1.output" portgid="0x505c656baf68b1c400000255000009a8" relation="Relation1"/>
  <link port="DS_handling#1.dsOut" portgid="0x505c7103af68b1c40000035a000009a8" relation="Relation2"/>
  <link port="PrintDS#1.InDS" portgid="0x505c70f7af68b1c40000034d000009a8" relation="Relation2"/>
</model>

