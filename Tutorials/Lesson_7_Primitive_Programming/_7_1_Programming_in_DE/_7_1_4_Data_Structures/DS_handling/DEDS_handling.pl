defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { DS_handling }
  domain      { DE }
  author      { Mission Level Design GmbH }
  version     { 0.0 09/21/2012 }
  copyright   { Mission Level Design GmbH }


  input
  {
    name { trigger }
    type { anytype }
  }

  output
  {
    name { dsOut }
    type { datastruct:Root }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  ccinclude { 
 
  //Load the kernel modules to access data structure methods
    <kernel/DataStructMember.h>
    <kernel/DsHandler.h> }

  protected
  {
    //define a pointer construct
    const TypeClass* mDataStruct;
  }

  setup
  {
    //set ds pointer to the predefinied data structure (use full name) 
    mDataStruct = DsHandler::findClassPointer("Root.Address.IPAddress");
  }

  go
  {
    //create new object of the structure
    TypeRef ds = mDataStruct->newValue();
    
    /* assign values field by field*/
    ds.setField("Byte1", 127);
    ds.setField("Byte2", 0);
    ds.setField("Byte3", 0);
    ds.setField("Byte4", 1);
    
    dsOut.put( arrivalTime ) << ds;
  }

}
