<?xml version='1.0' encoding='ISO-8859-1' ?>
<model gid="0x516e4ca16657f95400000431000013f0" name="system" version="3.1.0" xmlns="http://www.mldesigner.com/mld" xmlns:svg="http://www.w3.org/2000/svg">
  <svg:svg height="100%" width="100%">
    <property class="String" name="Bounding visible" value="false"/>
  </svg:svg>
  <parameter attributes="A_CONSTANT" gid="0x516e4ca16657f95400000433000013f0" name="GlobalSeed" scope="External" type="int" value="1234567890"/>
  <parameter attributes="A_CONSTANT" gid="0x516e4ca16657f95400000434000013f0" name="RunLength" scope="External" type="float" value="5">
    <property class="String" name="Description" value="Determines the number of cycles for non-timed domains or the time to run in seconds for timed domains. The maximum value is MAX_INT, i.e., 2^15-1. Value -1 can be used to define a pseudo-infinite runlength for domains that support the EndCondition feature by primitives."/>
  </parameter>
  <property class="String" name="Logical Name" value="system"/>
  <property class="String" name="Version" value="0.0 04/17/2013"/>
  <property class="String" name="Copyright" value="Mission Level Design GmbH"/>
  <property class="String" name="Author" value="Mission Level Design GmbH"/>
  <property class="String" name="hidden" value="no"/>
  <director class="DE" name="DE">
    <target class="default-DE">
      <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x516e4ca16657f95400000438000013f0" name="timeScale" scope="External" type="float" value="1.0">
        <property class="String" name="Description" value="Relative time scale for interface with another timed domain"/>
      </parameter>
      <parameter attributes="A_CONSTANT|A_SETTABLE" enumindices="" enumlabels="Calendar Queue Scheduler,Mutable Calendar Queue Scheduler,Priority Free Scheduler,Priority scheduler,Resource Contention scheduler,Simple DE scheduler" enumvalues="0,1,3,4,2,5" gid="0x516e4ca16657f95400000439000013f0" name="usedScheduler" scope="External" type="enum" value="3">
        <property class="String" name="Description" value="Specifies used DE scheduler, default is Priority Free Scheduler.&#10;"/>
      </parameter>
    </target>
  </director>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-553,220"/>
      <property class="String" name="Text" value="To open source code of a primitive right-click on it and choose 'open sorce code' or use shortcut 's'.&#10;&#10;The primitve 'AddInt_Example' contains a example for single ports and how to get Information from an input port and how to send via a output port.&#10;&#10;The primitve 'Fork_Example' contains a example for multi ports.&#10;&#10;The explanation is included in the source code."/>
      <property class="String" name="Alignment" value="centerleft"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-523,158"/>
      <property class="String" name="Font" value="Helvetica:12::::"/>
      <property class="String" name="Text" value="Description"/>
    </svg:svg>
  </label>
  <import name="_7_1_1___Port_access" url="../_7_1_1___Port_access.mml" urlgid="0x516e4bdf6657f9540000041d000013f0"/>
  <entity class="$MLD/MLD_Libraries/DE/Sources/Ramp/Ramp.mml" classgid="0x3dac05cd8b38f651000000ae00003aad" gid="0x516e4d2d6657f954000004aa000013f0" name="Ramp#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-216,72"/>
    </svg:svg>
    <property class="String" name="Description" value="Produce an output event with a monotic value when stimulated by an input event. The value of the output event starts at &quot;value&quot; and increases by &quot;step&quot; each time the star fires. The value of the input is ignored."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68878b38f651000083c200007914" gid="0x516e4d2d6657f954000004b0000013f0" name="value" scope="External" type="float" value="0.0">
      <property class="String" name="Description" value="Starting and current state of the ramp."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68878b38f651000083c300007914" gid="0x516e4d2d6657f954000004b1000013f0" name="step" scope="External" type="float" value="1.0">
      <property class="String" name="Description" value="Size of the ramp increments."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf68878b38f651000083c400007914" gid="0x516e4d2d6657f954000004ae000013f0" name="input" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-56,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
    </port>
    <port class="float" formalgid="0x3dcf68878b38f651000083c500007914" gid="0x516e4d2d6657f954000004af000013f0" name="output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="56,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="../AddInt_Example/AddInt_Example.mml" classgid="0x516e4ccd6657f95400000446000013f0" gid="0x516e4f726657f954000004ef000013f0" name="AddInt_Example#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-76,104"/>
    </svg:svg>
    <property class="String" name="Description" value="Computes the sum of the two input values."/>
    <port class="int" formalgid="0x516e4ccd6657f95400000448000013f0" gid="0x516e4f726657f954000004f1000013f0" name="Input1" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,-124"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,-124"/>
      </svg:svg>
    </port>
    <port class="int" formalgid="0x516e4ccd6657f95400000449000013f0" gid="0x516e4f726657f954000004f2000013f0" name="Input2" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,-84"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,-84"/>
      </svg:svg>
    </port>
    <port class="int" formalgid="0x516e4ccd6657f9540000044a000013f0" gid="0x516e4f726657f954000004f3000013f0" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="44,-100"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,-100"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/NumberGenerators/GenIntConst/GenIntConst.mml" classgid="0x3daa70498b38f6510000017a00004dd6" gid="0x516e4f956657f95400000513000013f0" name="GenIntConst#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-232,120"/>
    </svg:svg>
    <property class="String" name="Description" value="Outputs a constant integer value"/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf681e8b38f6510000709e00007914" gid="0x516e4f956657f95400000518000013f0" name="Value" scope="External" type="int" value="1">
      <property class="String" name="Description" value="The constant value."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf681e8b38f6510000709f00007914" gid="0x516e4f956657f95400000516000013f0" name="Trigger" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
    </port>
    <port class="int" formalgid="0x3dcf681e8b38f651000070a000007914" gid="0x516e4f956657f95400000517000013f0" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="44,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/Sinks/Printer/Printer.input=1.mml" classgid="0x3dac12598b38f651000004f300003aad" gid="0x516e550b6657f954000006b0000013f0" name="Printer.input=1#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="60,104"/>
    </svg:svg>
    <property class="String" name="Description" value="Print the value of each arriving event, together with its time of&#10;arrival.  The &quot;fileName&quot; parameter specifies the file to be written;&#10;the special names &amp;lt;stdout&amp;gt; and &amp;lt;out&amp;gt; (specifying the&#10;standard output stream), and &amp;lt;stderr&amp;gt; and &amp;lt;cerr&amp;gt;&#10;specifying the standard error stream, are also supported."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68808b38f6510000835b00007914" gid="0x516e550b6657f954000006b6000013f0" name="fileName" scope="External" type="filename" value="&lt;stdout&gt;">
      <property class="String" name="Description" value="Filename for output"/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68808b38f6510000835c00007914" gid="0x516e550b6657f954000006b7000013f0" name="Title" scope="External" type="string" value=""/>
    <port class="anytype" formalgid="0x3dcf68808b38f6510000835d00007914" gid="0x516e550b6657f954000006b5000013f0" name="input#1" type="input">
      <svg:svg height="100%" width="100%"/>
    </port>
  </entity>
  <entity class="../Fork_Example/Fork_Example.output=2.mml" classgid="0x516e54e46657f9540000063d000013f0" gid="0x516e55186657f954000006b8000013f0" name="Fork_Example.output=2#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-408,76"/>
    </svg:svg>
    <property class="String" name="Description" value="Replicates input events on the outputs with zero delay."/>
    <port class="anytype" formalgid="0x516e54e46657f9540000063e000013f0" gid="0x516e55186657f954000006bc000013f0" name="input" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
    </port>
    <port class="=input" formalgid="0x516e54e46657f95400000640000013f0" gid="0x516e55186657f954000006bd000013f0" name="output#1" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="44,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,0"/>
      </svg:svg>
    </port>
    <port class="=input" formalgid="0x516e54e46657f95400000641000013f0" gid="0x516e55186657f954000006be000013f0" name="output#2" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="44,48"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/DE/Sources/Clock/Clock.mml" classgid="0x3dac05cc8b38f651000000a600003aad" gid="0x516e5cbf6657f95400000742000013f0" name="Clock#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-536,76"/>
    </svg:svg>
    <property class="String" name="Description" value="&#10;Generate events at regular intervals, starting at time zero.&#10; "/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68858b38f651000083a100007914" gid="0x516e5cbf6657f95400000744000013f0" name="interval" scope="External" type="float" value="1.0">
      <property class="String" name="Description" value="The interval of events."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf68858b38f651000083a200007914" gid="0x516e5cbf6657f95400000745000013f0" name="magnitude" scope="External" type="float" value="1.0">
      <property class="String" name="Description" value="The value of the output particles generated."/>
    </parameter>
    <port class="float" formalgid="0x3dcf68858b38f651000083a300007914" gid="0x516e5cbf6657f95400000743000013f0" name="output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="24,-36"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-10,0"/>
      </svg:svg>
    </port>
  </entity>
  <relation name="Relation2">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-184,72 -136,100 -164,72 -164,100"/>
      <property class="String" name="Edges" value="0,2 1,3 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation3">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-184,120 -136,112 -164,112 -164,120"/>
      <property class="String" name="Edges" value="0,3 1,2 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation4">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-16,104 8,104"/>
      <property class="String" name="Edges" value="0,1"/>
    </svg:svg>
  </relation>
  <relation name="Relation5">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-332,72 -248,72"/>
      <property class="String" name="Edges" value="0,1"/>
    </svg:svg>
  </relation>
  <relation name="Relation6">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-332,84 -280,120 -304,84 -304,120"/>
      <property class="String" name="Edges" value="0,2 1,3 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-504,76 -484,76"/>
      <property class="String" name="Edges" value="0,1"/>
    </svg:svg>
  </relation>
  <link port="Ramp#1.output" portgid="0x516e4d2d6657f954000004af000013f0" relation="Relation2"/>
  <link port="AddInt_Example#1.Input1" portgid="0x516e4f726657f954000004f1000013f0" relation="Relation2"/>
  <link port="GenIntConst#1.Output" portgid="0x516e4f956657f95400000517000013f0" relation="Relation3"/>
  <link port="AddInt_Example#1.Input2" portgid="0x516e4f726657f954000004f2000013f0" relation="Relation3"/>
  <link port="AddInt_Example#1.Output" portgid="0x516e4f726657f954000004f3000013f0" relation="Relation4"/>
  <link port="Printer.input=1#1.input#1" portgid="0x516e550b6657f954000006b5000013f0" relation="Relation4"/>
  <link port="Fork_Example.output=2#1.output#1" portgid="0x516e55186657f954000006bd000013f0" relation="Relation5"/>
  <link port="Ramp#1.input" portgid="0x516e4d2d6657f954000004ae000013f0" relation="Relation5"/>
  <link port="Fork_Example.output=2#1.output#2" portgid="0x516e55186657f954000006be000013f0" relation="Relation6"/>
  <link port="GenIntConst#1.Trigger" portgid="0x516e4f956657f95400000516000013f0" relation="Relation6"/>
  <link port="Clock#1.output" portgid="0x516e5cbf6657f95400000743000013f0" relation="Relation1"/>
  <link port="Fork_Example.output=2#1.input" portgid="0x516e55186657f954000006bc000013f0" relation="Relation1"/>
</model>

