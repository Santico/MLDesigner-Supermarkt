defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { Fork_Example }
  domain      { DE }
  author      { Soonhoi Ha }
  version     { $Revision: 1.1 $ $Date: 2013/04/26 09:14:13 $ }
  copyright   { Copyright (c) 1990-1995 The Regents of the University of California. All rights reserved. See the file $MLD/copyright for copyright notice, limitation of liability, and disclaimer of warranty provisions. }

  desc
  {
    Replicates input events on the outputs with zero delay.
  }

  htmldoc
  {
    It is very important to pay attention to the processing sequence of the included ports which depends on numeration of the primitives ports.
  }


  input
  {
    name { input }
    type { anytype }
  }

  outmulti
  {
    name { output }
    type { =input }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  location { DE Control library }

  ccinclude {"kernel/Message.h","kernel/Type.h" }

  go
  {
    completionTime = arrivalTime;
    Particle& pp = input.get();
    Type *tType(0);
    //OutDEMPHIter = Output DE Multi Port Hole Iterator
    OutDEMPHIter nextp(output);
    OutDEPort *oport;

  //checks if it is a data structure
    bool isDS((pp.type() == MESSAGE || pp.type() == DATASTRUCTTYPE) &&
              (tType = (Type*)pp));
              
    //check all input ports          
    while ((oport = nextp++) != 0)
    {
      if (isDS) 
    //Data structures have to be cloned, if they will be used more then one time.
        oport->put(completionTime) << (Type*)tType->clone();
      else
    //Particles dont have to be cloned.
        oport->put(completionTime) << (Type*)pp;
    }
  }

}
