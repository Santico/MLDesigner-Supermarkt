defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { AddInt_Example }
  domain      { DE }
  author      { Tommy Baumann, Clemens Baron }
  version     { $Revision: 1.1 $ $Date: 2013/04/26 09:14:13 $ }
  copyright   { Copyright (C) 2003 MLDesign Technologies, Inc. }

  desc
  {
    Computes the sum of the two input values.
  }


  input
  {
    name { Input1 }
    type { int }
  }

  input
  {
    name { Input2 }
    type { int }
  }

  output
  {
    name { Output }
    type { int }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  acknowledge { }

  location { Arithmetic library }

  go
  {
    // '.dataNew' is TRUE if data arrives at the port. It can be reseted directly (Input1.dataNew=false) or with the '.get()' function of the port.
    if (Input1.dataNew && Input2.dataNew)
    {
      // Wth '.get()' you get the value which is arrived at the input port. It will be cast to int to be sure the right data type is used.
       int temp=(int)Input1.get();
      //'.put(time)<<' will output values. 'time' can be a value, the arrival time ('arrivalTime') or completion time ('completionTime').
       Output.put(arrivalTime) <<temp+(int)Input2.get();
       //You can access input port while outputting a value.
    }
  }

}
