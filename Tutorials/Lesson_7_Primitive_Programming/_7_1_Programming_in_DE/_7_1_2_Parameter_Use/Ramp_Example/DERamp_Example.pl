defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { Ramp_Example }
  domain      { DE }
  author      { Soonhoi Ha }
  version     { $Revision: 1.1 $ $Date: 2013/04/26 09:14:13 $ }
  copyright   { 
Copyright (c) 1990-1995 The Regents of the University of California.
All rights reserved.
See the file $MLD/copyright for copyright notice,
limitation of liability, and disclaimer of warranty provisions. }

  desc
  {
    Produce an output event with a monotic value when stimulated by an input event. The value of the output event starts at "value" and increases by "step" each time the star fires. The value of the input is ignored.
  }


  input
  {
    name { input }
    type { anytype }
  }

  output
  {
    name { output }
    type { float }
  }

  defparameter
  {
    name        { value }
    type        { float }
    default     { "0.0" }
    desc        { "Starting and current state of the ramp." }
  }

  defparameter
  {
    name        { step }
    type        { float }
    default     { "1.0" }
    desc        { "Size of the ramp increments." }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  location { DE main library }

  go
  {
	   // if the primitive is triggered, generate ramp output (ignore input).
	   completionTime = arrivalTime;
	   double t = value;
           output.put(completionTime) << t;
           //'step' is a paramerter and can be used like a variable. Parameter have to be created in the propertie editor before you can use them here.
		   t += double(step);
           value = t;
  }

}
