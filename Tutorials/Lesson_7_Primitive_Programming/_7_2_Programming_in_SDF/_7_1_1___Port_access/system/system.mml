<?xml version='1.0' encoding='ISO-8859-1' ?>
<model gid="0x516e8a906657f9540000093c000013d4" name="system" version="3.1.0" xmlns="http://www.mldesigner.com/mld" xmlns:svg="http://www.w3.org/2000/svg">
  <svg:svg height="100%" width="100%">
    <property class="String" name="Bounding visible" value="false"/>
  </svg:svg>
  <parameter attributes="A_CONSTANT" gid="0x516e4ca16657f95400000433000013f0" name="GlobalSeed" scope="External" type="int" value="1234567890"/>
  <parameter attributes="A_CONSTANT" gid="0x516e4ca16657f95400000434000013f0" name="RunLength" scope="External" type="float" value="5">
    <property class="String" name="Description" value="Determines the number of cycles for non-timed domains or the time to run in seconds for timed domains. The maximum value is MAX_INT, i.e., 2^15-1. Value -1 can be used to define a pseudo-infinite runlength for domains that support the EndCondition feature by primitives."/>
  </parameter>
  <property class="String" name="Logical Name" value="system"/>
  <property class="String" name="Version" value="0.0 04/17/2013"/>
  <property class="String" name="Copyright" value="Mission Level Design GmbH"/>
  <property class="String" name="Author" value="Mission Level Design GmbH"/>
  <property class="String" name="hidden" value="no"/>
  <director class="SDF" name="SDF">
    <target class="default-SDF">
      <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x516e8b8b6657f95400000974000013d4" name="logFile" scope="External" type="string" value="">
        <property class="String" name="Description" value="Log file to write to (none if empty)"/>
      </parameter>
      <parameter attributes="A_CONSTANT|A_SETTABLE" enumindices="" enumlabels="Acyloop Scheduler,Cluster Scheduler,Default Scheduler" enumvalues="2,1,0" gid="0x516e8b8b6657f95400000975000013d4" name="usedScheduler" scope="External" type="enum" value="0">
        <property class="String" name="Description" value="Specifies used DDF scheduler, default is Default Scheduler&#10;&#9;DEF - The default SDF scheduler&#10;&#9;CLUST - J. Buck's loop scheduler&#10;&#9;ACYLOOP - P. Murthy/S. Bhattacharyya's loop scheduler&#10;"/>
      </parameter>
      <parameter attributes="A_CONSTANT|A_SETTABLE" gid="0x516e8b8b6657f95400000976000013d4" name="schedulePeriod" scope="External" type="float" value="0.0">
        <property class="String" name="Description" value="schedulePeriod for interface with a timed domain."/>
      </parameter>
    </target>
  </director>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-553,232"/>
      <property class="String" name="Text" value="To open source code of a primitive right-click on it and choose 'open sorce code' or use shortcut 's'.&#10;&#10;The primitve 'Ramp_Example' contains a example for single ports and how to get Information from&#10;an input port and how to send via a output port.&#10;&#10;The primitve 'AddFloat_Example' contains a example for multi ports.&#10;&#10;The explanation is included in the source code."/>
      <property class="String" name="Alignment" value="centerleft"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-523,158"/>
      <property class="String" name="Font" value="Helvetica:12::::"/>
      <property class="String" name="Text" value="Description"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-543,314"/>
      <property class="String" name="Font" value="Helvetica:12::::"/>
      <property class="String" name="Text" value="Notes"/>
    </svg:svg>
  </label>
  <label>
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-556,337"/>
      <property class="String" name="Text" value="The use of parameters, memories, data structures and dynamic linking is the same as in the DE domain."/>
      <property class="String" name="Alignment" value="centerleft"/>
    </svg:svg>
  </label>
  <import name="_7_1_1___Port_access" url="../_7_1_1___Port_access.mml" urlgid="0x516e8a906657f9540000093e000013d4"/>
  <entity class="$MLD/MLD_Libraries/SDF/Sources/Const/Const.mml" classgid="0x3dac0a2b8b38f6510000011a00003aad" gid="0x516e8bc36657f95400000992000013d4" name="Const#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-496,56"/>
    </svg:svg>
    <property class="String" name="Description" value="Outputs a constant value defined by the parameter &quot;level&quot; (default 0.0)."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf552b8b38f6510000080c00007914" gid="0x516e8bc36657f95400000996000013d4" name="level" scope="External" type="float" value="0.0">
      <property class="String" name="Description" value="A constant value."/>
    </parameter>
    <port class="float" formalgid="0x3dcf552b8b38f6510000080d00007914" gid="0x516e8bc36657f95400000995000013d4" name="output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="24,0"/>
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-10,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="$MLD/MLD_Libraries/SDF/Sinks/Printer/Printer.input=1.mml" classgid="0x3dac0b0e8b38f6510000036600003aad" gid="0x516e8cb66657f95400000a80000013d4" name="Printer.input=1#2">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-144,76"/>
    </svg:svg>
    <property class="String" name="Description" value="Print out one sample from each input port per line."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf576b8b38f651000031cb00007914" gid="0x516e8cb66657f95400000a88000013d4" name="fileName" scope="External" type="filename" value="&lt;stdout&gt;">
      <property class="String" name="Description" value="Filename for output."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf576b8b38f651000031cc00007914" gid="0x516e8cb66657f95400000a89000013d4" name="Title" scope="External" type="string" value=""/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf576b8b38f651000031cd00007914" gid="0x516e8cb66657f95400000a8a000013d4" name="EndCondition" scope="External" type="boolean" value="FALSE">
      <property class="String" name="Description" value="If EndCondition is set to TRUE, the simulation will end when NumberOfItems &#10;have been consumed or when the number of cycles in Run Length have &#10;been executed, whichever comes first."/>
    </parameter>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x3dcf576b8b38f651000031ce00007914" gid="0x516e8cb66657f95400000a8b000013d4" name="NumberOfItems" scope="External" type="int" value="1">
      <property class="String" name="Description" value="The number of particles comsumed by an input port. If the primitive receives&#10;NumberOfItems input particles and the parameter EndCondition is set to TRUE,&#10;then the simulation will be finished."/>
    </parameter>
    <port class="anytype" formalgid="0x3dcf576b8b38f651000031cf00007914" gid="0x516e8cb66657f95400000a87000013d4" name="input#1" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="../Ramp_Example/Ramp_Example.mml" classgid="0x516e8d3b6657f95400000c14000013d4" gid="0x516e8d8e6657f95400000cb6000013d4" name="Ramp_Example#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-520,100"/>
    </svg:svg>
    <property class="String" name="Description" value="Generate a ramp signal, starting at &quot;value&quot; (default 0) and&#10;incrementing by step size &quot;step&quot; (default 1) on each firing."/>
    <parameter attributes="A_CONSTANT|A_SETTABLE" formalgid="0x516e8d3c6657f95400000c16000013d4" gid="0x516e8d8e6657f95400000cbb000013d4" name="step" scope="External" type="float" value="1.0">
      <property class="String" name="Description" value="Increment from one sample to the next."/>
    </parameter>
    <parameter attributes="A_NONCONSTANT|A_SETTABLE" formalgid="0x516e8d3c6657f95400000c17000013d4" gid="0x516e8d8e6657f95400000cbc000013d4" name="value" scope="External" type="float" value="0.0">
      <property class="String" name="Description" value="Initial (or latest) value output by Ramp."/>
    </parameter>
    <port class="float" formalgid="0x516e8d3c6657f95400000c18000013d4" gid="0x516e8d8e6657f95400000cba000013d4" name="output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="25,0"/>
        <property class="String" name="PortAlign" value="right"/>
      </svg:svg>
    </port>
  </entity>
  <entity class="../AddFloat_Example/AddFloat_Example.Input=2.mml" classgid="0x516e8d146657f95400000bcc000013d4" gid="0x516e8d996657f95400000cc1000013d4" name="AddFloat_Example.Input=2#1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Position" value="-320,76"/>
    </svg:svg>
    <property class="String" name="Description" value="Sum of floating inputs."/>
    <port class="float" formalgid="0x516e8d146657f95400000bce000013d4" gid="0x516e8d996657f95400000cc5000013d4" name="Output" type="output">
      <svg:svg height="0" width="0">
        <property class="String" name="PortAlign" value="right"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="34,0"/>
        <property class="String" name="Position" value="44,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Floating sum."/>
    </port>
    <port class="float" formalgid="0x516e8d146657f95400000bcf000013d4" gid="0x516e8d996657f95400000cc6000013d4" name="Input#1" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Floating addends."/>
    </port>
    <port class="float" formalgid="0x516e8d146657f95400000bd0000013d4" gid="0x516e8d996657f95400000cc7000013d4" name="Input#2" type="input">
      <svg:svg height="0" width="0">
        <property class="String" name="Position" value="-44,0"/>
        <property class="String" name="PortAlign" value="left"/>
        <property class="String" name="Rotation" value="0.0"/>
        <property class="String" name="ConnectPoint" value="-34,0"/>
      </svg:svg>
      <property class="String" name="Description" value="Floating addends."/>
    </port>
  </entity>
  <relation name="Relation1">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-464,100 -404,84 -444,100 -444,84"/>
      <property class="String" name="Edges" value="0,2 1,3 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation2">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-464,56 -404,72 -444,56 -444,72"/>
      <property class="String" name="Edges" value="0,2 1,3 2,3"/>
    </svg:svg>
  </relation>
  <relation name="Relation3">
    <svg:svg height="100%" width="100%">
      <property class="String" name="Vertices" value="-236,76 -196,76"/>
      <property class="String" name="Edges" value="0,1"/>
    </svg:svg>
  </relation>
  <link port="Ramp_Example#1.output" portgid="0x516e8d8e6657f95400000cba000013d4" relation="Relation1"/>
  <link port="AddFloat_Example.Input=2#1.Input#2" portgid="0x516e8d996657f95400000cc7000013d4" relation="Relation1"/>
  <link port="Const#1.output" portgid="0x516e8bc36657f95400000995000013d4" relation="Relation2"/>
  <link port="AddFloat_Example.Input=2#1.Input#1" portgid="0x516e8d996657f95400000cc6000013d4" relation="Relation2"/>
  <link port="AddFloat_Example.Input=2#1.Output" portgid="0x516e8d996657f95400000cc5000013d4" relation="Relation3"/>
  <link port="Printer.input=1#2.input#1" portgid="0x516e8cb66657f95400000a87000013d4" relation="Relation3"/>
</model>

