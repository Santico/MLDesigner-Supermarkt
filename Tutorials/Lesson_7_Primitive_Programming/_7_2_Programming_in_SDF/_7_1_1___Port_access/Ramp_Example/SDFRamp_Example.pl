defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { Ramp_Example }
  domain      { SDF }
  author      { D. G. Messerschmitt }
  version     { $Revision: 1.1 $ $Date: 2013/04/26 09:14:13 $ }
  copyright   { Copyright (c) 1990-1997 The Regents of the University of California.
All rights reserved.
See the file $MLD/copyright for copyright notice,
limitation of liability, and disclaimer of warranty provisions. }

  desc
  {
    Generate a ramp signal, starting at "value" (default 0) and
incrementing by step size "step" (default 1) on each firing.
  }


  output
  {
    name { output }
    type { float }
  }

  defparameter
  {
    name        { step }
    type        { float }
    default     { "1.0" }
    desc        { "Increment from one sample to the next." }
  }

  defparameter
  {
    name        { value }
    type        { float }
    default     { "0.0" }
    desc        { "Initial (or latest) value output by Ramp." }
    attrib      { A_NONCONSTANT|A_SETTABLE }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  location { SDF main library }

  go
  {
		double t = value;
    /*
     * The operator % operating on a porthole returns a reference to a particle.
     * The right-hand argument to the % operator specifies the delay of the access.
     * A zero always means the most recent particle. A one means the particle arriving just before the most recent particle. 
     */
		output%0 << t;
		t += step;
		value = t;
  }

}
