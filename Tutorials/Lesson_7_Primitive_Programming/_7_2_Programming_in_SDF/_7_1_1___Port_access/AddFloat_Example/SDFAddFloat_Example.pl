defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { AddFloat_Example }
  domain      { SDF }
  author      { J. T. Buck }
  version     { $Revision: 1.1 $ $Date: 2013/04/26 09:14:13 $ }
  copyright   { Copyright (c) 1990-1997 The Regents of the University of California.
All rights reserved.
See the file $MLD/copyright for copyright notice,
limitation of liability, and disclaimer of warranty provisions. }

  desc
  {
    Sum of floating inputs.
  }


  inmulti
  {
    name { Input }
    type { float }
    desc { "Floating addends." }
  }

  output
  {
    name { Output }
    type { float }
    desc { "Floating sum." }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  location { SDF main library }

  go
  {
    //create an iterator of the multi port hole
    //InSDFMPHIter = Input SDF Multi Port Hole Iterator
    InSDFMPHIter nexti(Input);
    InSDFPort *p;
    double sum = 0.0;
    
    // '.next()' returns a pointer to the next porthole in the list, until there are no more portholes
    while ((p = nexti.next()) != 0)
      sum += double((*p)%0);
    Output%0 << sum;
  }

}
