defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { exponentiate }
  domain      { SDF }
  author      { Mission Level Design GmbH }
  version     { 0.0 07/09/2012 }
  desc
  {
    This module calculates the output as base 'input a' to the power of 'input b'
  }


  input
  {
    name { A }
    type { int }
  }

  input
  {
    name { B }
    type { int }
  }

  output
  {
    name { Output }
    type { int }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  acknowledge { }

  location { }

  hinclude { }

  ccinclude { }

  constructor
  {
  }

  setup
  {
  }

  begin
  {
  }

  go
  {
     //%0 reads every tick the most recent particle
    int power = B%0;
    int base = A%0;
    
    // Exponentation
    int result = 1;
    for(int i = 0;i<=power;i++)
    {
      result = result*base;
    }
    // Proceed result as most recent output particle
    Output%0 << result;
  }

  wrapup
  {
  }

  cleanup
  {
  }

  destructor
  {
  }

}
