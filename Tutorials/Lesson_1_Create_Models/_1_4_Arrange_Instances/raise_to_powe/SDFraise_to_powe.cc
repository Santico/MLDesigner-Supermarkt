static const char file_id[] = "SDFraise_to_powe.pl";
// .cc file generated from SDFraise_to_powe.pl by ptlang

#ifndef CYGWIN
#ifdef __GNUC__
#pragma implementation
#endif
#endif

// needed for object file version handling
static char __version__[] = "__MLDesignerVersion__3.1.0";

#include "SDFraise_to_powe.h"
#include "kernel/SimControl.h"

const char *star_nm_SDFraise_to_powe = "SDFraise_to_powe";

ISA_FUNC(SDFraise_to_powe,SDFStar);

Block* SDFraise_to_powe :: makeNew() const { LOG_NEW; return new SDFraise_to_powe;}

Block& SDFraise_to_powe::setBlock(const char* s, Block* parent)
{
  Block& tBlock = SDFStar::setBlock(s,parent);
# ifdef COMPILE_WITH_DEBUG
    Error::warn(*this,"Primitive was compiled with debug information. This may lead to performance problems.");
# endif
  return tBlock;
}


SDFraise_to_powe::SDFraise_to_powe ()
{
	// needed for object file version handling
	char* tDummy = __version__; tDummy++;
	setDescriptor("This module calculates the output as base 'input a' to the power of 'input b'");
	addPort(base.setPort("base",this,FLOATTYPE));
	addPort(exponent.setPort("exponent",this,FLOATTYPE));
	addPort(result.setPort("result",this,FLOATTYPE));


}

void SDFraise_to_powe::go() {
# line 42 "SDFraise_to_powe.pl"
//1: Read the inputs
    //%0 reads most recent particleavailabe at port
    double t_base = base%0;
    double t_exponent = exponent%0;
    
    //2: Process data and calculate output
    double t_result = 0;
    t_result = pow(t_base,t_exponent);
    
    //3: Output result
    //Use operator "<<" to put result to output port
    result%0 << t_result;
}

// prototype instance for known block list
static SDFraise_to_powe proto;
static RegisterBlock registerBlock(proto,"raise_to_powe");
