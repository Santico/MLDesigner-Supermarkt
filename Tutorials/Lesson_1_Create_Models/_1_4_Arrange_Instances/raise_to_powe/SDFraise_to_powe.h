#ifndef _SDFraise_to_powe_h
#define _SDFraise_to_powe_h 1
// header file generated from SDFraise_to_powe.pl by ptlang

#ifndef CYGWIN
#ifdef __GNUC__
#pragma interface
#endif
#endif

#include "SDFStar.h"

class SDFraise_to_powe : public SDFStar
{
public:
	SDFraise_to_powe();
	/* virtual */ Block* makeNew() const;
	/* virtual */ int isA(const char*) const;
	/* virtual */ const char* className() const;
	/* virtual */ Block& setBlock(const char* s, Block* parent = NULL);
	InSDFPort base;
	InSDFPort exponent;
	OutSDFPort result;

protected:
	/* virtual */ void go();
};
#endif
