defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { raise_to_powe }
  domain      { SDF }
  author      { Mission Level Design GmbH }
  version     { 0.0 07/09/2012 }
  desc
  {
    This module calculates the output as base 'input a' to the power of 'input b'
  }


  input
  {
    name { base }
    type { float }
  }

  input
  {
    name { exponent }
    type { float }
  }

  output
  {
    name { result }
    type { float }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  go
  {
    //1: Read the inputs
    //%0 reads most recent particleavailabe at port
    double t_base = base%0;
    double t_exponent = exponent%0;
    
    //2: Process data and calculate output
    double t_result = 0;
    t_result = pow(t_base,t_exponent);
    
    //3: Output result
    //Use operator "<<" to put result to output port
    result%0 << t_result;
  }

}
