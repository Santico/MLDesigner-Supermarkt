// This file was generated by MLDesigner version 3.1.0

// importing SystemDS library for data structure definition
#include "$MLD/lib/system/SystemDS.cpp"

// importing libraries for data structure definition
#include "../_1_4_Arrange_Instances.cpp"



beginModule("submodule");
  setDomain("SDF");

  // definition of model parameters
  newParam("stopTime", "float", "$stopTime", false);
  newParam("curIter", "int", "$curIter", false);
  newParam("absIter", "int", "$absIter", false);
  newParam("absSimul", "int", "$absSimul", false);
  newParam("Parameter1", "int", "0", false);

  // definition of model memories

  // definition of model events

  // definition of model resources

  // definition of instances and their properties
  createInstance("AddConstFloat#1", "AddConstFloat", "", false);
  setParam("AddConstFloat#1",  "Addend", "$Parameter1");

  // define the connections
  alias("Input", "AddConstFloat#1", "Input");
  alias("Output", "AddConstFloat#1", "Output");

  // create sources and sinks for autoterminated ports
endModule();
