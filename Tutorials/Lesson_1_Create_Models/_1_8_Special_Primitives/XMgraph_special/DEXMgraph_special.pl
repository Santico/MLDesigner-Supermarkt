defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { XMgraph_special }
  domain      { DE }
  author      { E. A. Lee }
  version     { $Revision: 1.1 $ $Date: 2013/04/26 09:14:13 $ }
  copyright   { Copyright (c) 1990-1997 The Regents of the University of California.
All rights reserved.
See the file $MLD/copyright for copyright notice,
limitation of liability, and disclaimer of warranty provisions. }

  desc
  {
    Generate a plot with the pxgraph program with one point per event.
Any number of event sequences can be plotted simultaneously, up
to the limit determined by the XGraph class.
By default, a straight line is drawn between each pair of events.
  }


  inmulti
  {
    name { input }
    type { float }
  }

  defparameter
  {
    name        { title }
    type        { string }
    desc        { "Graph title." }
  }

  defparameter
  {
    name        { saveFile }
    type        { filename }
    desc        { "File to save the output to the pxgraph program." }
  }

  defparameter
  {
    name        { options }
    type        { string }
    default     { "-bb -tk -P =800x400" }
    desc        { "Command line options for the pxgraph program." }
  }

  defparameter
  {
    name        { Cumulation }
    type        { enum }
    enumvalues  { XG_CUM_NONE,XG_CUM_ITERS,XG_CUM_PMSETS }
    enumlabels  { None,Iterations,Paramsets }
    enumindices { 0,1,2 }
    default     { "XG_CUM_NONE" }
    desc        { "Specifies the cumulation level for the graph display:
None - a graph display for each iteration;
Iterations - a graph display for all iterations of a parameter set;
Paramsets - a single graph display for the whole simulation." }
  }

  defparameter
  {
    name        { Enabled }
    type        { boolean }
    default     { "TRUE" }
    desc        { "Enables or disables collecting/displaying data and writing files." }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  location { DE main library }

  seealso { $MLD/src/pxgraph/pxgraph.htm, Xhistogram }

  hinclude { "kernel/Display.h" }

  protected
  {
    XGraph graph;
  }

  virtual method
  {
    name    { generateLabel }
    access  { protected }
    arglist { "(StringState* pLabel)" }
    code
    {
      StringList tLabel;
      tLabel << fullName();
      if (!(*pLabel).null())
        tLabel << " : " << *pLabel;
      *pLabel = tLabel;
    }
  }


  setup
  {
    if (Enabled)
    {
      generateLabel(&title);
      graph.initialize(this,
                       input.numberPorts(),
                       (const char*) options,
                       (const char*) title,
                       (const char*) saveFile,
                       0,
                       Cumulation);
    }
  }

  go
  {
    if (Enabled)
    {
      InDEMPHIter nextp(input);
      InDEPort* p (0);
      int i (1);
      while ((p = nextp++))
      {
        if (p->dataNew)
        {
          graph.addPoint(i, arrivalTime, (float)p->get());
          p->dataNew = false;
        }
        ++i;
      }
    }
  }

  wrapup
  {
    if (Enabled)
      graph.terminate();
  }

}
