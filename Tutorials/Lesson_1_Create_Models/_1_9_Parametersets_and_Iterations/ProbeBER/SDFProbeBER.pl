defprimitive
{
  // !!! The following items are generated automatically.
  // !!! Changes of these items are lost after the modifi-
  // !!! cation of the primitive model

  name        { ProbeBER }
  domain      { SDF }
  author      { Gunar Schorcht, Clemens Baron }
  version     { $Revision: 1.1 $ $Date: 2013/04/26 09:14:13 $ }
  copyright   { Copyright (C) 2004 MLDesign Technologies, Inc. }


  input
  {
    name { BitError }
    type { int }
  }

  output
  {
    name { Output }
    type { int }
  }

  defparameter
  {
    name        { FileName }
    type        { filename }
    default     { "$MLD_USER/ProbeBER_${SIM_NO}.dat" }
  }

  defparameter
  {
    name        { EndCondition }
    type        { boolean }
    default     { "FALSE" }
    desc        { "If this parameter is set to TRUE, the primitive requests the simulation to halt after NumberOfBits tokens are collected" }
  }

  defparameter
  {
    name        { NumberOfBits }
    type        { int }
    default     { "1" }
    desc        { "Number of bits to collect, if EndCondition is set to TRUE" }
  }

  defparameter
  {
    name        { Parameter }
    type        { float }
    default     { " " }
    desc        { "Arbitrary real-valued parameter to be written to the file FileName" }
  }


  // !!! The following items are untouched by primitive
  // !!! model modifications. You can change them as
  // !!! you want.

  acknowledge { }

  location { }

  ccinclude { <stdio.h> "kernel/SimControl.h" }

  protected
  {
    int mErrors;
    int mCycles;
  }

  constructor
  {
  }

  destructor
  {
  }

  go
  {
    ++mCycles;
    if( EndCondition )
    {
      if( mCycles == (int)NumberOfBits )
        requestEnd(); // in this iteration, I have to collect the last bit.
      else if( mCycles > (int)NumberOfBits )
        return; // do not collect bits anymore.
    }

    mErrors += (int(BitError%0)) ? 1 : 0;
    Output%0 << double(mErrors)/double(mCycles);
  }

  setup
  {
    mCycles = 0;
    mErrors = 0;

    if (EndCondition) {
      willRequestEnd();
      if (NumberOfBits < 1)
        Error::abortRun(*this, 
                        " the value of the parameter NumberOfBits is less than 1");
    }
  }

  wrapup
  {
    const char* tFileMode = (SimControl::getAbsIter() == 1) ? "w" : "a";
    FILE*       tFilePtr  = fopen(FileName,tFileMode);

    if (tFilePtr)
    {      
      fprintf(tFilePtr, "%f %f\n", double(Parameter), double(mErrors)/double(mCycles));
      fclose(tFilePtr);
    }
  }

}
