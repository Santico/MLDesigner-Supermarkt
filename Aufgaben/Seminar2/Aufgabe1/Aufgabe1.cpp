// This file was generated by MLDesigner version 3.1.0


// importing SystemDS library for data structure definition
#include "$MLD/lib/system/SystemDS.cpp"

// importing libraries for data structure definition
#include "../Seminar2.cpp"

setDomain("DE");



defSystem("Aufgabe1");
  setDomain("DE");
  // define the target and set the target parameters
  setTarget("default-DE");
  setTargetParam("timeScale", "1.0");
  setTargetParam("usedScheduler", "3");

  // definition of model parameters
  newParam("GlobalSeed", "int", "1234567890", false);
  newParam("RunLength", "float", "100", false);

  // definition of model memories

  // definition of model events

  // definition of model resources

  // definition of instances and their properties
  createInstance("Clock#1", "Clock", "", false);
  setParam("Clock#1",  "interval", "1.0");
  setParam("Clock#1",  "magnitude", "1.0");
  createInstance("Impulse#1", "Impulse", "", false);
  setParam("Impulse#1",  "magnitude", "1.0");
  createInstance("Poisson#1", "Poisson", "", false);
  setParam("Poisson#1",  "MeanTime", "1.0");
  setParam("Poisson#1",  "Magnitude", "1.0");
  setParam("Poisson#1",  "Seed", "-1");
  createInstance("PulseGen#1", "PulseGen", "", false);
  setParam("PulseGen#1",  "value", "(0,1) (1,0) (2,5) (8,1)");
  createInstance("XMgraph.input=4#1", "XMgraph", "", false);
  numPorts("XMgraph.input=4#1",  "input", 4);
  setParam("XMgraph.input=4#1",  "title", "");
  setParam("XMgraph.input=4#1",  "saveFile", "");
  setParam("XMgraph.input=4#1",  "options", "-bb -tk -P =800x400");
  setParam("XMgraph.input=4#1",  "Cumulation", "0");
  setParam("XMgraph.input=4#1",  "Enabled", "TRUE");

  // define the connections
  connect("Clock#1", "output", "XMgraph.input=4#1", "input#1", "");
  connect("Impulse#1", "output", "XMgraph.input=4#1", "input#2", "");
  connect("Poisson#1", "output", "XMgraph.input=4#1", "input#3", "");
  connect("PulseGen#1", "output", "XMgraph.input=4#1", "input#4", "");

  // create sources and sinks for autoterminated ports


