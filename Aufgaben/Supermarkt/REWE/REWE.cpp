// This file was generated by MLDesigner version 3.1.0


// importing SystemDS library for data structure definition
#include "$MLD/lib/system/SystemDS.cpp"

// importing libraries for data structure definition
#include "../Supermarkt.cpp"

setDomain("DE");

#include "../Analyse/Analysebereich/Analysebereich.cpp"
#include "../Markt/Marktbereich/Marktbereich.cpp"
#include "../Kundengenerator/Kundengenerator/Kundengenerator.cpp"


defSystem("REWE");
  setDomain("DE");
  // define the target and set the target parameters
  setTarget("default-DE");
  setTargetParam("timeScale", "1.0");
  setTargetParam("usedScheduler", "3");

  // definition of model parameters
  newParam("GlobalSeed", "int", "-1", false);
  newParam("RunLength", "float", "1000", false);

  // definition of model memories
  newMemory("Einnahmen", "Internal", "Root", "Root{Root}");

  // definition of model events

  // definition of model resources

  // definition of instances and their properties
  createInstance("Analysebereich#1", "Analysebereich", "", false);
  setMemory("Analysebereich#1", "Einnahmen", "Root{Root}" ,"$Einnahmen");
  createInstance("Marktbereich#2", "Marktbereich", "", false);
  setMemory("Marktbereich#2", "EinnahmenGesamt", "Root{Root}" ,"$Einnahmen");
  createInstance("Kundengenerator#1", "Kundengenerator", "", false);
  setParam("Kundengenerator#1",  "AnzahlKunden", "100.0");
  setParam("Kundengenerator#1",  "Verrauschfaktor", "3.0");

  // define the connections
  connect("Marktbereich#2", "Marktausgang", "Analysebereich#1", "Kundenbefragung", "");
  connect("Kundengenerator#1", "Erstellter_Kunde", "Marktbereich#2", "Markteingang", "");

  // create sources and sinks for autoterminated ports


