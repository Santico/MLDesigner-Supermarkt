// This file was generated by MLDesigner version 3.1.0


// importing SystemDS library for data structure definition
#include "$MLD/lib/system/SystemDS.cpp"

// importing libraries for data structure definition
#include "../Leergut.cpp"
#include "../../../../Aufgaben.cpp"

setDomain("DE");

#include "../Leergutautomat/Leergutautomat.cpp"
#include "../../../Kundengenerator/KundenSchleife/KundenSchleife.cpp"


defSystem("Leergutautomat_Test");
  setDomain("DE");
  // define the target and set the target parameters
  setTarget("default-DE");
  setTargetParam("timeScale", "1.0");
  setTargetParam("usedScheduler", "3");

  // definition of model parameters
  newParam("GlobalSeed", "int", "1234567890", false);
  newParam("RunLength", "float", "1000", false);

  // definition of model memories

  // definition of model events

  // definition of model resources

  // definition of instances and their properties
  createInstance("Leergutautomat#1", "Leergutautomat", "", false);
  createInstance("SelectFieldDS#1", "SelectFieldDS", "", false);
  setParam("SelectFieldDS#1",  "dsFieldName", "Aufgaben:Root.Kunde#Zufriedenheit");
  createInstance("Xgraph#2", "Xgraph", "", false);
  setParam("Xgraph#2",  "title", "Zufriedenheit pro Kunde");
  setParam("Xgraph#2",  "saveFile", "");
  setParam("Xgraph#2",  "options", "-bb -tk -P =800x400");
  setParam("Xgraph#2",  "Cumulation", "0");
  setParam("Xgraph#2",  "Enabled", "YES");
  createInstance("VarDelay#1", "VarDelay", "", false);
  setParam("VarDelay#1",  "delay", "1");
  createInstance("Impulse#1", "Impulse", "", false);
  setParam("Impulse#1",  "magnitude", "1.0");
  createInstance("CreateDS#1", "CreateDS", "", false);
  setParam("CreateDS#1",  "dsName", "Aufgaben:Root.Kunde");
  createInstance("InsertFieldDS#1", "InsertFieldDS", "", false);
  setParam("InsertFieldDS#1",  "dsFieldName", "Aufgaben:Root.Kunde#Flaschen");
  createInstance("KundenSchleife#1", "KundenSchleife", "", false);
  setParam("KundenSchleife#1",  "gewuenschteAnzahl", "10.0");
  createInstance("Const#2", "Const", "", false);
  setParam("Const#2",  "Value", "10");

  // define the connections
  connect("SelectFieldDS#1", "Field", "Xgraph#2", "input", "");
  connect("CreateDS#1", "OutDS", "InsertFieldDS#1", "InDS", "");
  createInstance("auto-merge5", "Merge", "", false);
  connect("auto-merge5", "input", "Impulse#1", "output", "");
  connect("auto-merge5", "input", "KundenSchleife#1", "Output1", "");
  connect("auto-merge5", "output", "VarDelay#1", "input", "");
  connect("InsertFieldDS#1", "OutDS", "Leergutautomat#1", "Input1", "");
  connect("Leergutautomat#1", "Output1", "SelectFieldDS#1", "InDS", "");
  connect("Const#2", "Output", "InsertFieldDS#1", "Field", "");
  node("node7");

  nodeConnect("VarDelay#1", "output", "node7", "");
  nodeConnect("CreateDS#1", "Trigger", "node7", "");
  nodeConnect("KundenSchleife#1", "Input1", "node7", "");
  nodeConnect("Const#2", "Input", "node7", "");

  // create sources and sinks for autoterminated ports
  createInstance("OTerminator#8", "BlackHole", "", false);
  connect("SelectFieldDS#1", "OutDS", "OTerminator#8", "input", "");
  terminate("SelectFieldDS#1", "OutDS");
  createInstance("ITerminator#4", "Null", "", false);
  connect("ITerminator#4", "output", "VarDelay#1", "newDelay", "");
  terminate("VarDelay#1", "newDelay");


