/* This file was automaticaly generated. */
#ifndef Aufgaben_Kunde_h
#define Aufgaben_Kunde_h

#include "kernel/DsHandler.h"
#include "kernel/Root.h"
#include "kernel/BaseTypes.h"

/* Data structure class definition. */
/* The namespace is generated from the library name. */
namespace Aufgaben
{
class KundeR : public RootR
{
  public:
#define Kunde_INITS \
  ,Flaschen(mValue.getFieldRef(0))\
  ,Brotwaren(mValue.getFieldRef(1))\
  ,Wurstwaren(mValue.getFieldRef(2))\
  ,Kaesewaren(mValue.getFieldRef(3))\
  ,Zufriedenheit(mValue.getFieldRef(4))\
  ,KostenEinkauf(mValue.getFieldRef(5))

    //constructors
    DEFINEREFCONSTRUCTORS(Kunde, Root, Kunde_INITS)

    IntegerR Flaschen;
    IntegerR Brotwaren;
    IntegerR Wurstwaren;
    IntegerR Kaesewaren;
    FloatR Zufriedenheit;
    FloatR KostenEinkauf;

  /* Protected section */
  private:
    DEFINENEWVALUE("Aufgaben:Root.Kunde");

};// class definition
} // namespace

#endif
